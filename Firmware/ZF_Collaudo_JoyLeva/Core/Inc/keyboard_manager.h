/* 
 * File:   keyboard_mouse_manager.h
 * Author: firmware
 *
 * Created on December 18, 2019, 6:02 PM
 */

#ifndef KEYBOARD_MOUSE_MANAGER_H
#define	KEYBOARD_MOUSE_MANAGER_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "globals.h"

/***** PROTOTYPES ***/    
   
void SetRowsOutput(uint8_t nrow);
void SetColsInput(uint8_t ncol);
uint8_t  ReadColStatus(uint8_t ncol);
void Tx_Keys(void);
void Tx_Keys_ZF(void);
void Tx_Counter_ZF(void);
void SetLeds(void);
void SetLedsDisplay(void);
void ReadVarDisplay(void);
void RxLed_Init(void);
void RxLed_Process(void);
void Read_sensor(void);
void KeyboardScanRead(void);
void Receive_request_uart(uint8_t* Buf, uint32_t *Len);
void Zero_counter_set(uint8_t id);
void Send_count_answer(uint8_t id);//send  pressed-btn counter value for id key
void Send_ACK_answer(uint8_t cmd);

#ifdef	__cplusplus
}
#endif

#endif	/* KEYBOARD_MOUSE_MANAGER_H */

