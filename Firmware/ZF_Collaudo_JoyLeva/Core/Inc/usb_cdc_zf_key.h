/**
 * USB CDC Keyboard scan codes as per USB
 * plus some additional codes
 * 
 * Created by Paolo Valenti, 2021
 *
 * 
 * 
 */
 
#ifndef USB_CDC_ZF_KEYS
#define USB_CDC_ZF_KEYS


/*ZF LEVA and JOYSTICK*/
#define KEY_TRIMPORT_M_1  0x01 // Keyboard F1  ROW1 COL1
#define KEY_TRIMPORT_P_1  0x02 // Keyboard F2  ROW1 COL2
#define KEY_TRIMSTBD_M_1  0x03 // Keyboard F3  ROW1 COL3
#define KEY_TRIMSTBD_P_1  0x04 // Keyboard F4  ROW1 COL4

#define KEY_INCDEC_1      0x05 // Keyboard F5  ROW2 COL1
#define KEY_PIU_1         0x06 // Keyboard F6  ROW2 COL2
#define KEY_CTRL23_1      0x07 // Keyboard F7  ROW2 COL3

#define KEY_WARMUP_1      0x08 // Keyboard F8  ROW3 COL1
#define KEY_TROLL_1       0x09 // Keyboard F9  ROW3 COL2
#define KEY_MENO_1        0x0A // Keyboard F10 ROW3 COL3

#define KEY_CTRL1_1       0x0B // Keyboard F11 ROW4 COL1
#define KEY_ONELEVER_1    0x0C // Keyboard F12 ROW4 COL2


#define KEY_TRIMPORT_M_2  0x11 // Keyboard F1  ROW1 COL1
#define KEY_TRIMPORT_P_2  0x12 // Keyboard F2  ROW1 COL2
#define KEY_TRIMSTBD_M_2  0x13 // Keyboard F3  ROW1 COL3
#define KEY_TRIMSTBD_P_2  0x14 // Keyboard F4  ROW1 COL4

#define KEY_INCDEC_2      0x15 // Keyboard F5  ROW2 COL1
#define KEY_PIU_2         0x16 // Keyboard F6  ROW2 COL2
#define KEY_CTRL23_2      0x17 // Keyboard F7  ROW2 COL3

#define KEY_WARMUP_2      0x18 // Keyboard F8  ROW3 COL1
#define KEY_TROLL_2       0x19 // Keyboard F9  ROW3 COL2
#define KEY_MENO_2        0x1A // Keyboard F10 ROW3 COL3

#define KEY_CTRL1_2       0x1B // Keyboard F11 ROW4 COL1
#define KEY_ONELEVER_2    0x1C // Keyboard F12 ROW4 COL2


#define KEY_TRIMPORT_M_3  0x21 // Keyboard F1  ROW1 COL1
#define KEY_TRIMPORT_P_3  0x22 // Keyboard F2  ROW1 COL2
#define KEY_TRIMSTBD_M_3  0x23 // Keyboard F3  ROW1 COL3
#define KEY_TRIMSTBD_P_3  0x24 // Keyboard F4  ROW1 COL4

#define KEY_INCDEC_3      0x25 // Keyboard F5  ROW2 COL1
#define KEY_PIU_3         0x26 // Keyboard F6  ROW2 COL2
#define KEY_CTRL23_3      0x27 // Keyboard F7  ROW2 COL3

#define KEY_WARMUP_3      0x28 // Keyboard F8  ROW3 COL1
#define KEY_TROLL_3       0x29 // Keyboard F9  ROW3 COL2
#define KEY_MENO_3        0x2A // Keyboard F10 ROW3 COL3

#define KEY_CTRL1_3       0x2B // Keyboard F11 ROW4 COL1
#define KEY_ONELEVER_3    0x2C // Keyboard F12 ROW4 COL2


/*ZF JOYSTICK*/
#define KEY_JOY_BOOST_1    0x01 // Keyboard F1  ROW1 COL1
#define KEY_JOY_CTRL123_1  0x02 // Keyboard F2  ROW1 COL2

#define KEY_JOY_IANCHOR_1  0x03 // Keyboard F5  ROW2 COL1
#define KEY_JOY_THRUSTER_1 0x04 // Keyboard F6  ROW2 COL2


#define KEY_JOY_BOOST_2    0x11 // Keyboard F1  ROW1 COL1
#define KEY_JOY_CTRL123_2  0x12 // Keyboard F2  ROW1 COL2

#define KEY_JOY_IANCHOR_2  0x13 // Keyboard F5  ROW2 COL1
#define KEY_JOY_THRUSTER_2 0x14 // Keyboard F6  ROW2 COL2


#define KEY_JOY_BOOST_3    0x21 // Keyboard F1  ROW1 COL1
#define KEY_JOY_CTRL123_3  0x22 // Keyboard F2  ROW1 COL2

#define KEY_JOY_IANCHOR_3  0x23 // Keyboard F5  ROW2 COL1
#define KEY_JOY_THRUSTER_3 0x24 // Keyboard F6  ROW2 COL2

#endif //USB_CDC_ZF_KEY