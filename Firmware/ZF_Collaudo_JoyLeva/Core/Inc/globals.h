/*
 * globals.h
 *
 *  Created on: 07 ago 2019
 *      Author: VALENTI
 */

#ifndef INC_GLOBALS_H_
#define INC_GLOBALS_H_


#include "main.h"
#include <stdbool.h>
#include "ClosedCube_OPT3001.h"

/********* DEFINE ********/
#define FW_VER_BCD 0x0007
#define FW_VER  0x00
#define FW_SUB  0x07
#define USB_USE_HID_KEYBOARD

#define NUM_LED_RGB 6
#define SW1_LED_RGB 0
#define SW2_LED_RGB 1
#define SW3_LED_RGB 2
#define SW4_LED_RGB 3
#define SW5_LED_RGB 4
#define SW6_LED_RGB 5

#define LEN_RX_RS232 6
#define LEN_TX_RS232 6
#define RGB_DELTA_TRANSITION 615//15% duty //205 5% duty



#define NO_KEY_PRESSED          0u
#define LEFT_SINGLE_KEY_PRESSED 1u
#define RIGHT_DOUBLE_PRESSED    2u
#define KEY_LONG_PRESSED        3u




#define     PCA9955_ADDRESS1     0x65//0xC0 // 0x01//matteo
#define     PCA9955_ADDRESS2     0x69//0xC0 // 0x19//matteo
#define     AMB_LIGHT_ADDRESS     0x84//0xC0

#define     PCA9955_KEYB1_ADR1      0x65//0xC0
#define     PCA9955_KEYB1_ADR2      0x49//0xC0

#define     PCA9955_KEYB2_ADR1      0x65//0xC0
#define     PCA9955_KEYB2_ADR2      0x59//0xC0

#define     PCA9955_KEYB3_ADR1      0x65//0xC0
#define     PCA9955_KEYB3_ADR2      0x69//0xC0

/*LED JOYSTICK*/
#define LD15_BKL_CMIRROR_WHITE  15
#define LD14_NC                 14
#define LD13_NC                 13
#define LD12_NC                 12
#define LD11_WIFI_RED            11
#define LD10_WIFI_BLUE           10
#define LD9_THRUST_RED            9
#define LD8_THRUST_BLUE           8
#define LD7_IANCHOR_RED           7
#define LD6_IANCHOR_BLUE          6
#define LD5_RELEASE_RED           5
#define LD4_RELEASE_BLUE          4
#define LD3_BOOST_RED             3
#define LD2_BOOST_BLUE            2
#define LD1_TAKE_RED              1
#define LD0_TAKE_BLUE             0

/*LED BLUE LEVA */
#define LD15_BACKLIGHT                15
#define LD14_INCDEC_THROTTLE          14
#define LD13_NEUTRAL_STARBOARD        13
#define LD12_NEUTRAL2_PORT            12
#define LD11_ASTERN_STARBOARD         11
#define LD10_NEUTRAL1_STARBOARD       10
#define LD09_AHEAD_STARBOARD           9
#define LD08_AsTERN_PORT               8
#define LD07_NUTRAL1_PORT              7
#define LD06_AHEAD_PORT                6
#define LD05_RELEASE                   5
#define LD04_LOCK                      4
#define LD03_TAKE                      3
#define LD02_TROLL                     2
#define LD01_ONELEVER                  1
#define LD00_WARMUP                    0



/*LED DIGIT1 DIGIT2 LEVA*/
#define LD31_WIFIRED                  15
#define LD30_WIFIBLUE                 14
#define LD29_DIGIT1                   13
#define LD28_DIGIT1                   12
#define LD27_DIGIT1                   11
#define LD26_DIGIT1                   10
#define LD25_DIGIT1                    9
#define LD24_DIGIT1                    8
#define LD23_DIGIT1                    7
#define LD22_DIGIT2                    6
#define LD21_DIGIT2                    5
#define LD20_DIGIT2                    4
#define LD19_DIGIT2                    3
#define LD18_DIGIT2                    2
#define LD17_DIGIT2                    1
#define LD16_DIGIT2                    0






#define PCA9955_IREF_ALL       0xFF//0x80 gain current for all led output in PCA9955

#define OPT3001_ADR      0x44//I2C addres of slave OPT3001 ambient light sensor

/*MODBUS Registers*/
/* ----------------------- Static variables ---------------------------------*/

 /* Configuration */


#define KEYBOARD_UNLOCK     1
#define KEYBOARD_LOCK       0
#define LOGIC_POS           1
#define LOGIC_NEG           0

#define BUZZER_ON     1  //COMMENT if BUZZER is UNABLED
#define CHIAVE_ON     1  //COMMENT if CHIAVE is UNABLED
//#define CHIAVE_SEL  1  //UNCOMMENT if CHIAVE is a BISTABLE SELECTOR //COMMENT if CHIAVE is a BUTTON

#define CHIAVE_START_VALUE  KEYBOARD_UNLOCK
#define CHIAVE_LOGIC        LOGIC_POS



#define DELAY_BASE  40	// 212uS
#define MOV_THRS_OK	2

#define MAX_TASTI_CONTEMPORANEI  4


#define NUM_ROW_MAX      11
#define NUM_COL_MAX      10
#define NUM_BUTTON_MOUSE  2

#define NUM_ROW_ZF_MAX      12
#define NUM_COL_ZF_MAX      12


#define OUTPUT_PIN_PORT 0
#define INPUT_PIN_PORT  1

#define KEY_PRESSED_VALUE 0
#define KEY_PRESSED_DEBOUNCE_MS 1//10


#define KEYBOARD_TIMEOUT_IDLE 50000 //in milliseconds


#define BEEP_SHORT_PERIOD  18//8//20 //in 10ms
#define BEEP_SHORT_TOFF     5 //in 10ms

#define SENSOR_HALL_CLOSE  GPIO_PIN_SET
#define SENSOR_HALL_OPEN  GPIO_PIN_SET

/*PROTOCOL and VIRTUAL COM*/
#define NUM_BYTES_VCOM_TX  48//64//9
#define START_BYTE_POS  0
#define BITMAP3_HB_POS  1
#define BITMAP3_LB_POS  2
#define BITMAP2_HB_POS  3
#define BITMAP2_LB_POS  4
#define BITMAP1_HB_POS  5
#define BITMAP1_LB_POS  6
#define CHKSUM_HB_POS   7
#define CHKSUM_LB_POS   8

#define HEIDENHAIN_START_KEY_FRAME 0x80
#define START_BYTE_ALL_COUNTER_ZF  0x91
#define START_BYTE_SINGLE_COUNTER  0x90
#define NMAX_LED  32//10
#define LED_STATUS_ON       0x41    //'A'
#define LED_STATUS_OFF      0x53    //'S'
#define LED_STATUS_BLINK    0x4C    //'L'
#define READ_COUNTER_STATUS 0x52    //'R'
#define SET_COUNTER_VALUE   0x54    //'T'
#define ZERO_COUNTER_SET    0x5A    //'Z'
#define CONFIG_KEY_TEST     0x43    //'C'
#define SET_FRAME_RATE      0x46    //'F'
#define START_STOP_CMD      0x42    //'B'
#define ACK_ANSWER          0x06    //ACK

#define LED_BLINK_PERIOD_10ms 25  //250ms


#define ZF_LEVA_TEST           0x01
#define ZF_JOYSTICK_TEST       0x00
#define NUM_MAX_KEY_ZF    37

#define EEPROM_UNKNOW     0
#define EEPROM_24LC1025   1
#define EEPROM_24LC1024   2

#define NMAX_KEYPRESSED_TEST  3



/*******  VARIABLES KEYBOARD AND MOUSE MANAGEMENT ********/

/******* VARIABLES KEYBOARD ********/
typedef union
{
    uint32_t tot;
    struct
    {
        uint8_t LL1;
        uint8_t LH2;
        uint8_t HL3;
        uint8_t HH4;
    }b;
}count_t;

typedef struct
{
    //uint8_t pin;
    //uint8_t port;
    uint8_t val;
    uint8_t deb_ms;
    uint8_t old;

    //uint8_t chr;
} keys_keyboard_t;


 typedef struct
 {
        uint8_t numLock        :1;
        uint8_t capsLock       :1;
        uint8_t scrollLock     :1;
        uint8_t compose        :1;
        uint8_t kana           :1;
        uint8_t                :3;
 }keys_leds_t;

 typedef struct
 {
        uint8_t active              :1;
        uint8_t start               :1;
        uint8_t start_off           :1;
        uint8_t nbeep               :5;
        uint16_t period_10ms;
        uint16_t count_down_10ms;
        uint16_t t_on_10ms;
        uint16_t t_off_10ms;
 }buzzer_t;

 typedef struct
 {
        uint8_t active                    :1;
        uint8_t btn0_sel1                 :1;
        uint8_t logic                     :1;
        uint8_t status                    :1;
        uint8_t actual                    :1;
        uint8_t old                       :1;
        uint8_t free                      :2;
        uint16_t deb;
        uint16_t del_start;//
 }switch_t;
 typedef union
{
    uint16_t    bitmap;
    struct
    {
        uint8_t b0: 1;
        uint8_t b1: 1;
        uint8_t b2: 1;
        uint8_t b3: 1;
        uint8_t b4: 1;
        uint8_t b5: 1;
        uint8_t b6: 1;
        uint8_t b7: 1;
        uint8_t b8: 1;
        uint8_t b9: 1;
        uint8_t b10: 1;
        uint8_t b11: 1;
        uint8_t b12: 1;
        uint8_t b13: 1;
        uint8_t b14: 1;
        uint8_t b15: 1;
    }b;
    struct
    {
        uint8_t LB;
        uint8_t HB;
    } byte;
}keys_word_t;


typedef union
{
    uint8_t    bitmap;
    struct
    {
        uint8_t POR: 1;//Power on reset
        uint8_t COM_ERR: 1;
        uint8_t I2C_ERR: 1;
        uint8_t STOP0_RUN1: 1;
        uint8_t ERR1: 1;
        uint8_t ERR2: 1;
        uint8_t b6: 1;
        uint8_t b7: 1;
    }b;

}status_byte_t;


typedef union
{
    uint8_t    byte;
    struct
    {
        uint8_t START: 1;//Power on reset
        uint8_t STOP : 1;
        uint8_t HALL_UP: 1;
        uint8_t HALL_DOWN: 1;
        uint8_t BTN1: 1;
        uint8_t UP_OLD: 1;
        uint8_t DOWN_OLD: 1;
        uint8_t JOY_LEVA: 1;
    }b;

}command_byte_t;

typedef struct
{
    uint16_t us100;
    uint16_t ms;
    uint16_t ms10;
    uint16_t ms100;
    uint8_t  sec;
    uint8_t  min;
    uint32_t hour;
    bool flag_100us;
    bool flag_10ms;
    bool flag_100ms;
    bool flag_1sec;
    bool flag_1min;
    bool flag_50ms;
    bool flag_10sec;
} system_time_struct_t;

typedef struct
{
 uint8_t buffer_rx[3];
 uint8_t pos_byte_rx;
 uint16_t timeout_rx_ms;
 uint16_t timeout_blink_10ms;
 uint8_t  toggle_led_blink;
 uint8_t rx_status;
 uint8_t status_led[NMAX_LED];
 uint16_t brightness;
 uint16_t bright_old;
}led_type_t;


typedef enum {
			ID_TXT =0,
			COUNTS_TXT =1,
			TOT_TXT=2,
			TASTO1_TXT=3,
			TASTO2_TXT=4,
			TASTO3_TXT=5,
			STATUS_TXT=6,
			VP_MAX_TXT=16,
}msg_pos_enum_t;

typedef enum {
			COUNT_TOT_VAR =0,
			COUNT_1_VAR =1,
			COUNT_2_VAR =2,
			COUNT_3_VAR =3,
			KEYB_TYPE_VAR=4,
			TASTO1_LEVA_ICO_VAR  = 5,
			TASTO2_LEVA_ICO_VAR  = 6,
			TASTO3_LEVA_ICO_VAR  = 7,
			TASTO4_LEVA_ICO_VAR  = 8,
			TASTO5_LEVA_ICO_VAR  = 9,
			TASTO6_LEVA_ICO_VAR  = 10,
			TASTO7_LEVA_ICO_VAR  = 11,
			TASTO8_LEVA_ICO_VAR  = 12,
			TASTO9_LEVA_ICO_VAR  = 13,
			TASTO10_LEVA_ICO_VAR  = 14,
			TASTO11_LEVA_ICO_VAR  = 15,
			TASTO12_LEVA_ICO_VAR  = 16,
			TASTO1_JOY_ICO_VAR  = 17,
			TASTO2_JOY_ICO_VAR  = 18,
			TASTO3_JOY_ICO_VAR  = 19,
			TASTO4_JOY_ICO_VAR  = 20,
			TASTO5_JOY_ICO_VAR  = 21,
			TASTO6_JOY_ICO_VAR  = 22,
			TASTO7_JOY_ICO_VAR  = 23,
			TASTO8_JOY_ICO_VAR  = 24,
			TASTO9_JOY_ICO_VAR  = 25,
			TASTO10_JOY_ICO_VAR  =26,
			TASTO11_JOY_ICO_VAR  = 27,
			TASTO12_JOY_ICO_VAR  = 28,
			TASTO_LED_PRESS_VAR  = 29,
			LIGHT_SENSOR_VAR     = 30,
			LED_BRIGHT_VAR       = 31,
			VP_MAX_VAR=40,
}var_pos_enum_t;

typedef struct{
   uint16_t task;
   uint16_t menu_id;
   uint16_t vp[10];
   uint16_t vp_txt[VP_MAX_TXT];
   uint16_t vp_var[VP_MAX_VAR];
   char  txt[20];
   uint16_t row_id;
   uint16_t nrow;
   uint8_t  len_txt;
   uint8_t  shot_id;
   uint8_t  lang;
   uint8_t  Transfer_cplt;
   uint8_t RX[10];
}display_t;


 keys_keyboard_t kb_status[NUM_ROW_ZF_MAX][NUM_COL_ZF_MAX];

 uint8_t num_keys_pressed;
 uint8_t keys_multi[MAX_TASTI_CONTEMPORANEI];
 keys_leds_t keys_special;
 keys_word_t keys_tx[3];
 keys_word_t keys_tx_old[3];
 uint8_t  id_kp;
 buzzer_t buz1;
 switch_t chiave;

 led_type_t led;
 led_type_t led_usb;
 uint8_t uart1_data_rx;
 uint8_t Tx_Message[NUM_BYTES_VCOM_TX];
/******* VARIABLES SYSTEM TIMER********/
uint16_t duty_pwm_backlight;


uint16_t keyboardIdleRateMillisec;
//uint8_t  PCA9685_delay_event
uint8_t opt3001_rd_time;
uint8_t  scan_read_period_ms;
uint8_t  scan_row_delay;
system_time_struct_t main_clock;
uint8_t  m_ADDR;
uint8_t  test_type;
uint8_t ee24xx1025_adr;
uint8_t eeprom_type;
uint8_t config_id_key[NMAX_KEYPRESSED_TEST];
keys_word_t system_flag;
count_t count_press_ZF[NUM_MAX_KEY_ZF];
status_byte_t machine_status;
uint16_t frame_rate_tx;
uint16_t tx_counter;
command_byte_t sensor;
display_t LCM1;
#endif /* INC_GLOBALS_H_ */
