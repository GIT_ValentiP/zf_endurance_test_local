/*
 ******************************************************************************
 *  @file      : display.h
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 14 apr 2021
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : NUOVA_BN_RS485_CAN 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

#ifndef INC_DISPLAY_H_
#define INC_DISPLAY_H_


/* Includes ----------------------------------------------------------*/
#include "globals.h"
#include "usart.h"


/* Typedef -----------------------------------------------------------*/

/* Define ------------------------------------------------------------*/


/* Macro -------------------------------------------------------------*/


/* Variables ---------------------------------------------------------*/



/* Function prototypes -----------------------------------------------*/
void disp_init(void);
void disp_pic(uint16_t id);
void disp_txt(uint16_t vp,uint16_t pic_id,char *txt,uint8_t length);
void disp_ico(uint16_t vp,uint16_t pic_id,uint16_t val);
void disp_data(uint16_t vp,uint32_t val,uint8_t len_int,uint8_t len_dec);
uint8_t disp_read_data(uint16_t vp,uint8_t *rd,uint8_t len_int);
void disp_rtc(uint16_t vp,uint16_t year,uint8_t month,uint8_t day,uint8_t hour,uint8_t min);
void disp_set_rtc(uint16_t vp,uint16_t year,uint8_t month,uint8_t day,uint8_t hour,uint8_t min);
void display_update_data(void);
void display_update_txt(void);
void display_update_ico(uint8_t scan_code,uint16_t val);

#endif /* INC_DISPLAY_H_ */
