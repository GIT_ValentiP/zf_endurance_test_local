/*
 *****************************************************************************
 *  @file      : PCA9955.c
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 30 mag 2020
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : Sanremo_REVO3 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

/* Includes ----------------------------------------------------------*/

#include "PCA9955.h"
#include "i2c.h"
#include "globals.h"
#include <string.h>
/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
char    init_array[] = {
    0x80,                                           //  Command
    0x00, 0x05,                                     //  MODE1, MODE2
    0xAA, 0xAA, 0xAA, 0xAA,                         //  LEDOUT[3:0]
    0x80, 0x00,                                     //  GRPPWM, GRPFREQ
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //  PWM[7:0]
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //  PWM[15:8]
    0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, //  IREF[7:0]
    0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, //  IREF[15:8]
    0x08                                            //  OFFSET: 1uS offsets
};


/* Private function prototypes -----------------------------------------------*/
 void PCA9955_delay(uint16_t millisecond);
// void m_I2C_write(uint16_t addr, char* buffer ,uint16_t len);

// void m_I2C_read(uint16_t addr, char* buffer ,uint16_t len);
/* Private user code ---------------------------------------------------------*/

 void m_I2C_write(uint16_t addr, char* buffer ,uint16_t len){
	 HAL_I2C_Master_Transmit(&hi2c1,addr <<1,(uint8_t *)buffer,len,1000);
	// HAL_I2C_Master_Transmit(&hi2c2,addr <<1,(uint8_t *)buffer,len,1000);
 }

 void m_I2C_read(uint16_t addr, char* buffer ,uint16_t len){
	 uint16_t adr_rd=(addr<<1);
	 adr_rd =(adr_rd|0x0001);
	 //HAL_I2C_Master_Receive(&hi2c1,adr_rd,(uint8_t *)buffer,len,1000);
	 HAL_I2C_Master_Receive(&hi2c1,adr_rd,(uint8_t *)buffer,len,1000);
 }


 char PCA9955_read(char reg)
 {
	 //HAL_StatusTypeDef status;
	 //Select the register
     m_I2C_write(m_ADDR, &reg, 1);

     I2C_Delay_us(10);

     //if(HAL_I2C_IsDeviceReady(&hi2c1,m_ADDR<<1,3, 10)==HAL_OK){
     //Read the 8-bit register
     m_I2C_read(m_ADDR, &reg, 1);
     //}
     //Return the byte
     return reg;
 }

 void PCA9955_write(int addr, char reg, char data)
 {
     //Create a temporary buffer
     char buff[2];

     //Load the register address and 8-bit data
     buff[0] = reg;
     buff[1] = data;

     //Write the data
     m_I2C_write(addr, buff, 2);
 }

 void PCA9955_readMulti(char startReg, char* data, int length)
 {
     //Select the starting register
     m_I2C_write(m_ADDR, &startReg, 1);

     //Read the specified number of bytes
     m_I2C_read(m_ADDR, data, length);
 }

 void PCA9955_writeMulti(int addr, char* data, int length)
 {
     //Write the data
     m_I2C_write(addr, data, length);
 }


void PCA9955_init(void)
{
   /*
    *
    *
#define I2C_RESET1_Pin GPIO_PIN_0
#define I2C_RESET1_GPIO_Port GPIOB
#define I2C_RESET2_Pin GPIO_PIN_1
#define I2C_RESET2_GPIO_Port GPIOB
#define I2C_RESET3_Pin GPIO_PIN_2
#define I2C_RESET3_GPIO_Port GPIOB
    */


	HAL_GPIO_WritePin(I2C_RESET1_GPIO_Port,I2C_RESET1_Pin,GPIO_PIN_SET); //Pin NRESET stay HIGH ,if 0V the reset chipset
	HAL_GPIO_WritePin(I2C_RESET2_GPIO_Port,I2C_RESET2_Pin,GPIO_PIN_SET); //Pin NRESET stay HIGH ,if 0V the reset chipset
	HAL_GPIO_WritePin(I2C_RESET3_GPIO_Port,I2C_RESET3_Pin,GPIO_PIN_SET); //Pin NRESET stay HIGH ,if 0V the reset chipset

	PCA9955_reset();

	m_ADDR=PCA9955_ADDRESS1;
	PCA9955_setallOutputCurrents_u8(led.brightness, m_ADDR);//(PCA9955_IREF_ALL, m_ADDR);

	m_ADDR=PCA9955_ADDRESS2;
	PCA9955_setallOutputCurrents_u8(led.brightness, m_ADDR);//(PCA9955_IREF_ALL, m_ADDR);
	//PCA9955_setallOutputStates(OUTPUT_ON,m_ADDR);
}

//#define LD15_BKL_CMIRROR_WHITE  15
//#define LD14_NC                 14
//#define LD13_NC                 13
//#define LD12_NC                 12
//#define LD11_WIFI_RED            11
//#define LD10_WIFI_BLUE           10
//#define LD9_THRUST_RED           9
//#define LD8_THRUST_BLUE           8
//#define LD7_IANCHOR_RED           7
//#define LD6_IANCHOR_BLUE          6
//#define LD5_RELEASE_RED           5
//#define LD4_RELEASE_BLUE          4
//#define LD3_BOOST_RED             3
//#define LD2_BOOST_BLUE            2
//#define LD1_TAKE_RED              1
//#define LD0_TAKE_BLUE             0



void PCA9955_LED_ANIMATION(int cycle){
   char data[6];
   OutputState status[2];
   switch(cycle){
	   case(0):

	            PCA9955_setoutputState(LD15_BKL_CMIRROR_WHITE, OUTPUT_ON);
				break;
	   case(1):
		        PCA9955_setoutputState(LD15_BKL_CMIRROR_WHITE, OUTPUT_ON);
	            PCA9955_setoutputState(LD0_TAKE_BLUE, OUTPUT_ON);
				break;
	   case(2):
			    PCA9955_setoutputState(LD0_TAKE_BLUE, OUTPUT_OFF);
		        PCA9955_setoutputState(LD1_TAKE_RED, OUTPUT_ON);

				break;
	   case(3):
	        	PCA9955_setoutputState(LD1_TAKE_RED, OUTPUT_OFF);
				PCA9955_setoutputState(LD2_BOOST_BLUE, OUTPUT_ON);
				break;
	   case(4):
		        PCA9955_setoutputState(LD2_BOOST_BLUE, OUTPUT_OFF);
				PCA9955_setoutputState(LD3_BOOST_RED, OUTPUT_ON);
	        	break;
	   case(5):
		        PCA9955_setoutputState(LD3_BOOST_RED, OUTPUT_OFF);
				PCA9955_setoutputState(LD4_RELEASE_BLUE, OUTPUT_ON);
				break;
	   case(6):
		        PCA9955_setoutputState(LD4_RELEASE_BLUE, OUTPUT_OFF);
			    PCA9955_setoutputState(LD5_RELEASE_RED, OUTPUT_ON);
				break;
	   case(7):
		        PCA9955_setoutputState(LD5_RELEASE_RED, OUTPUT_OFF);
			    PCA9955_setoutputState(LD6_IANCHOR_BLUE, OUTPUT_ON);
				break;
	   case(8):
		        PCA9955_setoutputState(LD6_IANCHOR_BLUE, OUTPUT_OFF);
			    PCA9955_setoutputState(LD7_IANCHOR_RED, OUTPUT_ON);

				break;
	   case(9):
		        PCA9955_setoutputState(LD7_IANCHOR_RED, OUTPUT_OFF);
//			    PCA9955_setoutputState(LD8_THRUST_BLUE, OUTPUT_ON);
			    data[0]=0x84;//Turn All led on
			    data[1]=0x01;
			    m_I2C_write(m_ADDR, data, 2);
	            break;
	   case(10):
		     //   PCA9955_setoutputState(LD8_THRUST_BLUE, OUTPUT_OFF);
		        data[0]=0x84;//Turn All led on
				data[1]=0x00;
				m_I2C_write(m_ADDR, data, 2);
			    PCA9955_setoutputState(LD9_THRUST_RED, OUTPUT_ON);

	   		    break;
	   case(11):
//   	        PCA9955_setoutputState(LD9_THRUST_RED, OUTPUT_OFF);
				        data[0]=0x84;//Turn All led on
						data[1]=0x00;
						m_I2C_write(m_ADDR, data, 2);
				//PCA9955_setoutputState(LD10_WIFI_BLUE, OUTPUT_ON);
				data[0]=0x84;//Turn All led on
				data[1]=0x08;
				m_I2C_write(m_ADDR, data, 2);

	   		    break;
	   case(12):
//	 		        PCA9955_setoutputState(LD10_WIFI_BLUE, OUTPUT_OFF);
		            data[0]=0x84;//Turn All led on
					data[1]=0x00;
					m_I2C_write(m_ADDR, data, 2);
    				//PCA9955_setoutputState(LD11_WIFI_RED, OUTPUT_ON);
					 data[0]=0x84;//Turn All led on
					data[1]=0x08;
					m_I2C_write(m_ADDR, data, 2);
	 	   		    break;
	   case(13):
	 		        //PCA9955_setoutputState(LD11_WIFI_RED, OUTPUT_OFF);
	                PCA9955_setallOutputStates(OUTPUT_ON,m_ADDR);
	 	   		    break;
	   case(14):
		            //PCA9955_setallOutputStates(OUTPUT_OFF,m_ADDR);
		            status[0]=OUTPUT_OFF;
	                PCA9955_setOutputStates(status,LD0_TAKE_BLUE,LD11_WIFI_RED);
	                data[0]=0x84;//Turn All led on
					data[1]=0x00;
					m_I2C_write(m_ADDR, data, 2);
	 	 	   	    break;
	   default:
		        PCA9955_setoutputState(LD15_BKL_CMIRROR_WHITE, OUTPUT_ON);
	   		    break;
   }

}

bool PCA9955_open()
{
    //Probe for the PCA9952/55 using a Zero Length Transfer
//    if (!m_I2C_write(m_ADDR, NULL, 0)) {
//        //Return success
        return true;
//    } else {
//        //Return failure
//        return false;
//    }
}

void PCA9955_reset()
{
    //The General Call Reset command
    char data = 0x06;

    //Issue the command to the General Call address
    m_I2C_write(0x00, &data, 1);
}

bool PCA9955_getallCallEnabled()
{
    //Read the 8-bit register value
    char value = PCA9955_read(REG_MODE1);

    //Return the status of the ALLCALL bit
    if (value & (1 << 0))
        return true;
    else
        return false;
}

void PCA9955_setallCallEnabled(bool enabled)
{
    //Read the current 8-bit register value
    char value = PCA9955_read(REG_MODE1);

    //Set or clear the ALLCALL bit
    if (enabled)
        value |= (1 << 0);
    else
        value &= ~(1 << 0);

    //Write the value back out
    PCA9955_write(m_ADDR, REG_MODE1, value);
}

bool PCA9955_getsubCall3Enabled()
{
    //Read the 8-bit register value
    char value = PCA9955_read(REG_MODE1);

    //Return the status of the SUB3 bit
    if (value & (1 << 1))
        return true;
    else
        return false;
}

void PCA9955_setsubCall3Enabled(bool enabled)
{
    //Read the current 8-bit register value
    char value = PCA9955_read(REG_MODE1);

    //Set or clear the SUB3 bit
    if (enabled)
        value |= (1 << 1);
    else
        value &= ~(1 << 1);

    //Write the value back out
    PCA9955_write(m_ADDR, REG_MODE1, value);
}

bool PCA9955_getsubCall2Enabled()
{
    //Read the 8-bit register value
    char value = PCA9955_read(REG_MODE1);

    //Return the status of the SUB2 bit
    if (value & (1 << 2))
        return true;
    else
        return false;
}

void PCA9955_setsubCall2Enabled(bool enabled)
{
    //Read the current 8-bit register value
    char value = PCA9955_read(REG_MODE1);

    //Set or clear the SUB2 bit
    if (enabled)
        value |= (1 << 2);
    else
        value &= ~(1 << 2);

    //Write the value back out
    PCA9955_write(m_ADDR, REG_MODE1, value);
}

bool PCA9955_getsubCall1Enabled()
{
    //Read the 8-bit register value
    char value = PCA9955_read(REG_MODE1);

    //Return the status of the SUB1 bit
    if (value & (1 << 3))
        return true;
    else
        return false;
}

void PCA9955_setsubCall1Enabled(bool enabled)
{
    //Read the current 8-bit register value
    char value = PCA9955_read(REG_MODE1);

    //Set or clear the SUB1 bit
    if (enabled)
        value |= (1 << 3);
    else
        value &= ~(1 << 3);

    //Write the value back out
    PCA9955_write(m_ADDR, REG_MODE1, value);
}

PowerMode PCA9955_getpowerMode()
{
    //Read the 8-bit register value
    char value = PCA9955_read(REG_MODE1);

    //Return the status of the SLEEP bit
    if (value & (1 << 4))
        return POWER_SHUTDOWN;
    else
        return POWER_NORMAL;
}

void PCA9955_setpowerMode(PowerMode mode)
{
    //Read the current 8-bit register value
    char value = PCA9955_read(REG_MODE1);

    //Set or clear the SLEEP bit
    if (mode == POWER_SHUTDOWN)
        value |= (1 << 4);
    else
        value &= ~(1 << 4);

    //Write the value back out
    PCA9955_write(m_ADDR, REG_MODE1, value);
}

OutputChangeMode PCA9955_getoutputChangeMode()
{
    //Read the 8-bit register value
    char value = PCA9955_read(REG_MODE2);

    //Return the status of the OCH bit
    if (value & (1 << 3))
        return OUTPUT_CHANGE_ON_ACK;
    else
        return OUTPUT_CHANGE_ON_STOP;
}

void PCA9955_setoutputChangeMode(OutputChangeMode mode)
{
    //Read the current 8-bit register value
    char value = PCA9955_read(REG_MODE2);

    //Set or clear the OCH bit
    if (mode == OUTPUT_CHANGE_ON_ACK)
        value |= (1 << 3);
    else
        value &= ~(1 << 3);

    //Write the value back out
    PCA9955_write(m_ADDR, REG_MODE2, value);
}

GroupMode PCA9955_getgroupMode()
{
    //Read the 8-bit register value
    char value = PCA9955_read(REG_MODE2);

    //Return the status of the DMBLNK bit
    if (value & (1 << 5))
        return GROUP_BLINKING;
    else
        return GROUP_DIMMING;
}

void PCA9955_setgroupMode(GroupMode mode)
{
    //Read the current 8-bit register value
    char value = PCA9955_read(REG_MODE2);

    //Set or clear the DMBLNK bit
    if (mode == GROUP_BLINKING)
        value |= (1 << 5);
    else
        value &= ~(1 << 5);

    //Write the value back out
    PCA9955_write(m_ADDR, REG_MODE2, value);
}

bool PCA9955_overTemp()
{
    //Read the current 8-bit register value
    char value = PCA9955_read(REG_MODE2);

    //Return the status of the OVERTEMP bit
    if (value & (1 << 7))
        return true;
    else
        return false;
}

OutputState PCA9955_getoutputState(Output output)
{
    char value;
    char reg;

    //Determine which register to read
    if (output < 4) {
        reg = REG_LEDOUT0;
    } else if (output < 8) {
        output = (Output)(output - 4);
        reg = REG_LEDOUT1;
    } else if (output < 12) {
        output = (Output)(output - 8);
        reg = REG_LEDOUT2;
    } else {
        output = (Output)(output - 12);
        reg = REG_LEDOUT3;
    }

    //Read the 8-bit register value
    value = PCA9955_read(reg);

    //Shift and mask the other output states
    value = (value >> (output * 2)) & 0x03;

    //Return the selected output's state
    if (value == 0)
        return OUTPUT_OFF;
    else if (value == 1)
        return OUTPUT_ON;
    else if (value == 2)
        return OUTPUT_PWM;
    else
        return OUTPUT_PWM_GRPPWM;
}

void PCA9955_setoutputState(Output output, OutputState state)
{
    char value;
    char reg;

    //Determine which register to read
    if (output < 4) {
        reg = REG_LEDOUT0;
    } else if (output < 8) {
        output = (Output)(output - 4);
        reg = REG_LEDOUT1;
    } else if (output < 12) {
        output = (Output)(output - 8);
        reg = REG_LEDOUT2;
    } else {
        output = (Output)(output - 12);
        reg = REG_LEDOUT3;
    }

    //Read the current 8-bit register value
    value = PCA9955_read(reg);

    //Mask off the old output state (also turns the output off)
    value &= ~(0x03 << (output * 2));

    //Add the new output state
    if (state == OUTPUT_ON)
        value |= (1 << (output * 2));
    else if (state == OUTPUT_PWM)
        value |= (2 << (output * 2));
    else if (state == OUTPUT_PWM_GRPPWM)
        value |= (3 << (output * 2));

    //Write the value back out
    PCA9955_write(m_ADDR, reg, value);
}

char PCA9955_getgroupDuty_u8()
{
    //Return the 8-bit register value
    return PCA9955_read(REG_GRPPWM);
}

float PCA9955_getgroupDuty()
{
    //Return the value as a float
    return PCA9955_getgroupDuty_u8() / 255.0;
}


void PCA9955_setgroupDuty_u8(char duty, int altAddr)
{
    //Write the new 8-bit register value
    PCA9955_write(altAddr, REG_GRPPWM, duty);
}

void PCA9955_setgroupDuty(float duty, int altAddr)
{
    //Range check the value
    if (duty < 0.0)
        duty = 0.0;
    if (duty > 1.0)
        duty = 1.0;

    //Convert the value to a char and write it
    PCA9955_setgroupDuty_u8((char)(duty * 255.0), altAddr);
}




float PCA9955_getgroupBlinkPeriod()
{
    //Read the 8-bit register value
    char value = PCA9955_getgroupBlinkPeriod_u8();

    //Return the period in seconds
    if (value == 0x00)
        return 0.067;
    else if (value == 0xFF)
        return 16.8;
    else
        return (value + 1) / 15.26;
}
char PCA9955_getgroupBlinkPeriod_u8()
{
    //Return the 8-bit register value
    return PCA9955_read(REG_GRPFREQ);
}

void PCA9955_setgroupBlinkPeriod_u8(char period, int altAddr)
{
    //Write the new 8-bit register value

    PCA9955_write(altAddr, REG_GRPFREQ, period);

}
void PCA9955_setgroupBlinkPeriod(float period, int altAddr)
{
    char value = 0;

    //Do a smart conversion
    if (period > 0.067) {
        if (period < 16.8)
            value = (char)((period * 15.26) - 1);
        else
            value = 0xFF;
    }

    //Write the new 8-bit register value
    PCA9955_setgroupBlinkPeriod_u8(value, altAddr);
}



float PCA9955_getoutputDuty(Output output)
{
    //Return the value as a float
    return PCA9955_getoutputDuty_u8(output) / 255.0;
}

char PCA9955_getoutputDuty_u8(Output output)
{
    //Return the 8-bit register value
    return PCA9955_read(REG_PWM0 + (char)output);
}

void PCA9955_setoutputDuty_u8(Output output, char duty, int altAddr)
{
    //PCA9955_write the new 8-bit register value
    PCA9955_write(altAddr, REG_PWM0 + (char)output, duty);
}

void PCA9955_setoutputDuty(Output output, float duty, int altAddr)
{
    //Range check the value
    if (duty < 0.0)
        duty = 0.0;
    if (duty > 1.0)
        duty = 1.0;

    //Convert the value to a char and write it
    PCA9955_setoutputDuty_u8(output, (char)(duty * 255.0), altAddr);
}

char PCA9955_getoutputCurrent_u8(Output output)
{
    //Return the 8-bit register value
    return PCA9955_read(REG_IREF0 + (char)output);
}

void PCA9955_setoutputCurrent_u8(Output output, char iref, int altAddr)
{
    //Write the new 8-bit register value
    PCA9955_write(altAddr, REG_IREF0 + (char)output, iref);
}

float PCA9955_getoutputCurrent(Output output)
{
    //Return the value as a float
    return PCA9955_getoutputCurrent_u8(output) / 255.0;
}

void PCA9955_setoutputCurrent(Output output, float iref, int altAddr)
{
    //Range check the value
    if (iref < 0.0)
        iref = 0.0;
    if (iref > 1.0)
        iref = 1.0;

    //Convert the value to a char and write it
    PCA9955_setoutputCurrent_u8(output, (char)(iref * 255.0), altAddr);
}



char PCA9955_getoutputDelay()
{
    //Return the 8-bit register value (minus the top 4 bits)
    return PCA9955_read(REG_OFFSET) & 0x0F;
}

void PCA9955_setoutputDelay(char clocks, int altAddr)
{
    //Write the new 8-bit register value (minus the top 4 bits)
    PCA9955_write(altAddr, REG_OFFSET, clocks & 0x0F);
}

char PCA9955_getsubCall1Addr()
{
    //Return the 8-bit address
    return PCA9955_read(REG_SUBADR1);
}

void PCA9955_setsubCall1Addr(char addr, int altAddr)
{
    //Write the new 8-bit address
    PCA9955_write(altAddr, REG_SUBADR1, addr);
}

char PCA9955_getsubCall2Addr()
{
    //Return the 8-bit address
    return PCA9955_read(REG_SUBADR2);
}

void PCA9955_setsubCall2Addr(char addr, int altAddr)
{
    //Write the new 8-bit address
    PCA9955_write(altAddr, REG_SUBADR2, addr);
}

char PCA9955_getsubCall3Addr()
{
    //Return the 8-bit address
    return PCA9955_read(REG_SUBADR3);
}

void PCA9955_setsubCall3Addr(char addr, int altAddr)
{
    //Write the new 8-bit address
    PCA9955_write(altAddr, REG_SUBADR3, addr);
}

char PCA9955_getallCallAddr()
{
    //Return the 8-bit address
    return PCA9955_read(REG_ALLCALLADR);
}

void PCA9955_setallCallAddr(char addr, int altAddr)
{
    //Write the new 8-bit address
    PCA9955_write(altAddr, REG_ALLCALLADR, addr);
}

void PCA9955_getOutputStates(OutputState* states, Output start, Output end)
{
    //Read the range of output states
    for (int i = 0; i < end - start + 1; i++) {
        states[i] = PCA9955_getoutputState((Output)(start + i));
    }
}

void PCA9955_setOutputStates(OutputState* states, Output start, Output end)
{
    //Set the range of output states
    for (int i = 0; i < end - start + 1; i++) {
    	PCA9955_setoutputState((Output)(start + i), states[0]);//states[i]);
    }
}

void PCA9955_setallOutputStates(OutputState state, int altAddr)
{
    char buff[5];

    //Assemble the sending array
    buff[0] = REG_LEDOUT0 | REG_AUTO_INC;
    if (state == OUTPUT_OFF) {
        memset(buff + 1, 0x00, 4);
    } else if (state == OUTPUT_ON) {
        memset(buff + 1, 0x55, 4);
    } else if (state == OUTPUT_PWM) {
        memset(buff + 1, 0xAA, 4);
    } else {
        memset(buff + 1, 0xFF, 4);
    }

    //Send the array
    PCA9955_writeMulti( altAddr, buff, 5);
}
void PCA9955_setallOutputDuties_u8(char duty, int altAddr)
{
    //Write the new 8-bit register value
    PCA9955_write(altAddr, REG_PWMALL, duty);
}


void PCA9955_getOutputDuties_u8(char* duties, Output start, Output end)
{
    //Read all of the duty cycles at once
	PCA9955_readMulti((REG_PWM0 + start) | REG_AUTO_INC, duties, end - start + 1);
}

void PCA9955_setOutputDuties_u8(char* duties, Output start, Output end, int altAddr)
{
    char buff[end - start + 2];

    //Assemble the sending array
    buff[0] = (REG_PWM0 + start) | REG_AUTO_INC;
    memcpy(buff + 1, duties, end - start + 1);

    //Send the array
    PCA9955_writeMulti(altAddr, buff, end - start + 2);
}

void PCA9955_getOutputDuties(float* duties, Output start, Output end)
{
    char buff[end - start + 1];

    //Read the range of the duty cycles as unsigned chars first
    PCA9955_getOutputDuties_u8(buff, start, end);

    //Convert all of the duty cycles to percents
    for (int i = 0; i < end - start + 1; i++) {
        duties[i] = buff[i] / 255.0;
    }
}

void PCA9955_setOutputDuties(float* duties, Output start, Output end, int altAddr)
{
    char buff[end - start + 2];

    //Assemble the sending array
    buff[0] = (REG_PWM0 + start) | REG_AUTO_INC;
    for (int i = 1; i < end - start + 2; i++) {
        //Range check the value
        if (duties[i - 1] < 0.0)
            duties[i - 1] = 0.0;
        if (duties[i - 1] > 1.0)
            duties[i - 1] = 1.0;

        //Convert the value to a char and write it
        buff[i] = duties[i - 1] * 255.0;
    }

    //Send the array
    PCA9955_writeMulti(altAddr, buff, end - start + 2);
}

void PCA9955_setallOutputDuties(float duty, int altAddr)
{
    //Range check the value
    if (duty < 0.0)
        duty = 0.0;
    if (duty > 1.0)
        duty = 1.0;

    //Convert the value to a char and write it
    PCA9955_setallOutputDuties_u8((char)(duty * 255.0), altAddr);
}

void PCA9955_getOutputCurrents_u8(char* irefs, Output start, Output end)
{
    //Read all of the current references at once
	PCA9955_readMulti((REG_IREF0 + start) | REG_AUTO_INC, irefs, end - start + 1);
}

void PCA9955_setOutputCurrents_u8(char* irefs, Output start, Output end, int altAddr)
{
    char buff[end - start + 2];

    //Assemble the sending array
    buff[0] = (REG_IREF0 + start) | REG_AUTO_INC;
    memcpy(buff + 1, irefs, end - start + 1);

    //Send the array
    PCA9955_writeMulti(altAddr, buff, end - start + 2);
}

void PCA9955_setallOutputCurrents_u8(char iref, int altAddr)
{
    //Write the new 8-bit register value
    PCA9955_write( altAddr, REG_IREFALL, iref);
}


void PCA9955_getOutputCurrents(float* irefs, Output start, Output end)
{
    char buff[end - start + 1];

    //Read all of the current references as unsigned chars first
    PCA9955_getOutputCurrents_u8(buff, start, end);

    //Convert all of the duty cycles to percents
    for (int i = 0; i < end - start + 1; i++) {
        irefs[i] = buff[i] / 255.0;
    }
}

void PCA9955_setOutputCurrents(float* irefs, Output start, Output end, int altAddr)
{
    char buff[end - start + 2];

    //Assemble the sending array
    buff[0] = (REG_IREF0 + start) | REG_AUTO_INC;
    for (int i = 1; i < end - start + 2; i++) {
        //Range check the value
        if (irefs[i - 1] < 0.0)
            irefs[i - 1] = 0.0;
        if (irefs[i - 1] > 1.0)
            irefs[i - 1] = 1.0;

        //Convert the value to a char and write it
        buff[i] = irefs[i - 1] * 255.0;
    }

    //Send the array
    PCA9955_writeMulti(altAddr, buff, end - start + 2);
}

void PCA9955_allOutputCurrents(float iref, int altAddr)
{
    //Range check the value
    if (iref < 0.0)
        iref = 0.0;
    if (iref > 1.0)
        iref = 1.0;

    //Convert the value to a char and write it
    PCA9955_setallOutputCurrents_u8((char)(iref * 255.0), altAddr);
}


unsigned short PCA9955_faultTest()
{
    //Read the current 8-bit register value
    char value = PCA9955_read(REG_MODE2);

    //Set the FAULTTEST bit
    value |= (1 << 6);

    //Write the value back out
    PCA9955_write(m_ADDR, REG_MODE2, value);

    //Wait for the fault test to complete
    while (PCA9955_read(REG_MODE2) & (1 << 6));

    //Read the lower 8 flags
    unsigned short flags = PCA9955_read(REG_EFLAG0);

    //Add the upper 8 flags
    flags |= PCA9955_read(REG_EFLAG1) << 8;

    //Return the combined flags
    return flags;
}

void I2C_Delay_us(uint16_t n){
	uint16_t i=0;
	for(i=0;i<n;i++);
}
void PCA9955_AmbientLight_reg(void){

	uint32_t amb_lux;
	uint32_t delta;
	amb_lux=((uint32_t)opt3001_sensor.lux);
	if(opt3001_sensor.lux>lux_old){
		delta=((uint32_t)opt3001_sensor.lux-(uint32_t)lux_old);
	}else{
		delta=((uint32_t)lux_old-(uint32_t)opt3001_sensor.lux);
	}
	if(delta > 3){
//		if(amb_lux<1000){
//	    amb_lux=((amb_lux * 254)/1000);
//		}else if(amb_lux<2000){
			if(amb_lux>1500){
				amb_lux=1500;
			}
		    amb_lux=((amb_lux * 250)/1500);
//			}else{
//			    amb_lux=((amb_lux * 254)/3000);
//				}

	    amb_lux=(255-amb_lux);
	    PCA9955_setallOutputCurrents_u8((char)amb_lux, m_ADDR);

	}
	lux_old=opt3001_sensor.lux;

}

void PCA9955_Set_led(uint8_t id,uint8_t status_led){
	uint8_t id_rel;
		id_rel=id;
	  	if (id_rel<32){

	  		//if(test_type==ZF_LEVA_TEST){
				if (id_rel<16){
					m_ADDR=PCA9955_ADDRESS1;
					//id_rel=id;
				}else{
					id_rel=id_rel-16;
					m_ADDR=PCA9955_ADDRESS2;
				}
	  		//}else{
	  			//id_rel=(id_rel % 16);
	  			//m_ADDR=PCA9955_ADDRESS1;
	  		//}
	  	   switch(status_led){
	  	                      case(LED_STATUS_ON):
			                                        PCA9955_setoutputState(id_rel, OUTPUT_ON);//OUT00_SetLow();
	  	                                            break;
	  	                      case(LED_STATUS_BLINK):
	  	                                            if(led.toggle_led_blink){
	  	                                            	PCA9955_setoutputState(id_rel, OUTPUT_ON);//OUT00_SetLow();
	  	                                            }else{
	  	                                            	PCA9955_setoutputState(id_rel, OUTPUT_OFF);//OUT00_SetHigh();
	  	                                            }
	  	                                            break;
	  	                      default:
	  	                    	       PCA9955_setoutputState(id_rel, OUTPUT_OFF);//OUT00_SetHigh();
	  	                               break;
	  	                  }
	  	}
}
