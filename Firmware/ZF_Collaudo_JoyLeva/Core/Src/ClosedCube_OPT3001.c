/*

Arduino library for Texas Instruments OPT3001 Digital Ambient Light Sensor
Written by AA for ClosedCube
---

The MIT License (MIT)

Copyright (c) 2015 ClosedCube Limited

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/
/* Includes ----------------------------------------------------------*/

#include "ClosedCube_OPT3001.h"
#include "i2c.h"
#include "globals.h"
#include <string.h>
#include <math.h>
#include <stdio.h>
/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/


void OPT3001_init(void)
{
	OPT3001_Config newConfig;
	OPT3001_ErrorCode errorConfig;
	//OPT3001_Config sensorConfig ;
    //uint16_t id_sensor;


	OPT3001_address = OPT3001_ADR;
	//HAL_I2C_Master_Transmit(&hi2c1,OPT3001_address <<1,0x06,1,250);
	//OPT3001_begin(OPT3001_address);

	newConfig.RangeNumber = 0b1100;
	newConfig.ConvertionTime = 0b1;
	newConfig.Latch = 0b1;
	newConfig.ModeOfConversionOperation = 0b11;

    //id_sensor=OPT3001_readManufacturerID();
    //printf("Manufacturer ID:%x \n\r",id_sensor);
    //id_sensor=OPT3001_readDeviceID();
    //printf("Serial numb.:%d \n\r",id_sensor);

	errorConfig = OPT3001_writeConfig(newConfig);

	if (errorConfig != NO_ERROR)
	{
		  //  printf("OPT3001 config.err:%d \n\r", errorConfig);
	}
//	else{

//			sensorConfig = OPT3001_readConfig();
//			printf("OPT3001 Config:\n\r");
//			printf("------------------------------");

//			printf("Conversion ready (R):%d \n\r",sensorConfig.ConversionReady);
//
//			printf("Conv.time :%d\n\r",sensorConfig.ConvertionTime);//
//			printf(sensorConfig.ConvertionTime, HEX);
//
//			printf("Fault cnt:%d\n\r",sensorConfig.FaultCount);//printf("Fault count field (R/W):");
//			printf(sensorConfig.FaultCount, HEX);
//
//			printf("Flag high field:%d\n\r",sensorConfig.FlagHigh);//printf("Flag high field (R-only):");
//			printf(sensorConfig.FlagHigh, HEX);
//
//			printf("Flag low field:%d\n\r",sensorConfig.FlagLow);//printf("Flag low field (R-only):");
//			printf(sensorConfig.FlagLow, HEX);
//
//			printf("Latch field:%d\n\r",sensorConfig.Latch);//printf("Latch field (R/W):");
//			printf(sensorConfig.Latch, HEX);
//
//			printf("Mask exponent field :%d\n\r",sensorConfig.MaskExponent);//printf("Mask exponent field (R/W):");
//			printf(sensorConfig.MaskExponent, HEX);
//
//			printf("Mode of conversion operation:%d\n\r",sensorConfig.ModeOfConversionOperation);//printf("Mode of conversion operation (R/W):");
//			printf(sensorConfig.ModeOfConversionOperation, HEX);
//
//			printf("Polarity field:%d\n\r",sensorConfig.Polarity);//printf("Polarity field (R/W):");
//			printf(sensorConfig.Polarity, HEX);
//
//			printf("Overflow flag:%d\n\r",sensorConfig.OverflowFlag);//printf("Overflow flag (R-only):");
//			printf(sensorConfig.OverflowFlag, HEX);
//
//			printf("Range number:%d\n\r",sensorConfig.RangeNumber);//printf("Range number (R/W):");
//			printf(sensorConfig.RangeNumber, HEX);

//			printf("--");
//		}


}

void configureSensor() {


}



OPT3001_ErrorCode OPT3001_begin(uint16_t address) {
	OPT3001_ErrorCode err = NO_ERROR;
	//OPT3001_address = address;
	uint16_t adr_rd;
	  uint16_t id_sensor;
	//Wire.begin();
	uint8_t buff[3] ;
	uint8_t buff_rx[2];
	HAL_StatusTypeDef er_i2c;
	//buff[0] = 0x06;
    //Issue the command to the General Call address
	//er_i2c=HAL_I2C_Master_Transmit(&hi2c1,0x0000,buff,1,1000);
	buff[0] = MANUFACTURER_ID;
	adr_rd=(OPT3001_ADR);
	er_i2c=HAL_I2C_Master_Transmit(&hi2c1,adr_rd<<1,buff,1,1000);
	adr_rd=((OPT3001_ADR<<1)|0x0001);
	er_i2c=HAL_I2C_Master_Receive(&hi2c1,adr_rd,buff_rx,2,1000);
	id_sensor=((buff_rx[0]<<8)|buff_rx[1]);
	printf("Manufacturer ID:%x \n\r",id_sensor);
	if(er_i2c!= HAL_OK){
	  		err=TIMEOUT_ERROR ;
	}
	return (err);//NO_ERROR;
}

uint16_t OPT3001_readManufacturerID() {
	uint16_t result = 0xFFFF;//0;
	OPT3001_ErrorCode error = OPT3001_writeData(MANUFACTURER_ID);
	if (error == NO_ERROR){
		error = OPT3001_readData(&result);

	}
	return result;
}

uint16_t OPT3001_readDeviceID() {
	uint16_t result = 0xFFFF;
	OPT3001_ErrorCode error = OPT3001_writeData(DEVICE_ID);
	if (error == NO_ERROR){
		error = OPT3001_readData(&result);
	}
	return result;
}

OPT3001_Config OPT3001_readConfig() {
	OPT3001_Config config;
	OPT3001_ErrorCode error = OPT3001_writeData(CONFIG);
	if (error == NO_ERROR){
		error = OPT3001_readData(&config.rawData);
	}
	return config;
}

OPT3001_ErrorCode OPT3001_writeConfig(OPT3001_Config config) {
//	Wire.beginTransmission(_address);
//	Wire.write(CONFIG);
//	Wire.write(config.rawData >> 8);
//	Wire.write(config.rawData & 0x00FF);
//  return (OPT3001_ErrorCode)(-10 * Wire.endTransmission());
	OPT3001_ErrorCode err=NO_ERROR ;
	HAL_StatusTypeDef er_i2c;
	uint16_t word_tx;
	uint8_t	buf[3];
	word_tx=(config.rawData >> 8);
	buf[0]=CONFIG;
	buf[1]=(uint8_t)word_tx;
	buf[2]=(uint8_t)config.rawData;
	er_i2c=HAL_I2C_Master_Transmit(&hi2c1,OPT3001_address<<1,buf,3,1000);
	if(er_i2c!= HAL_OK){
		err=TIMEOUT_ERROR ;
	}
    return(err);

}

OPT3001 OPT3001_readResult() {
	return OPT3001_readRegister(RESULT);
}

OPT3001 OPT3001_readHighLimit() {
	return OPT3001_readRegister(HIGH_LIMIT);
}

OPT3001 OPT3001_readLowLimit() {
	return OPT3001_readRegister(LOW_LIMIT);
}

OPT3001 OPT3001_readRegister(OPT3001_Commands command) {
	OPT3001_ErrorCode error = OPT3001_writeData(command);
	if (error == NO_ERROR) {
		OPT3001 result;
		result.lux = 0;
		result.error = NO_ERROR;

		OPT3001_ER er;
		error = OPT3001_readData(&er.rawData);
		if (error == NO_ERROR) {
			result.raw = er;
			result.lux = 0.01*pow(2, er.Exponent)*er.Result;
		}
		else {
			result.error = error;
		}

		return result;
	}
	else {
		return OPT3001_returnError(error);
	}
}

OPT3001_ErrorCode OPT3001_writeData(OPT3001_Commands command)
{
	OPT3001_ErrorCode err=NO_ERROR ;
	HAL_StatusTypeDef er_i2c;
	uint8_t buf[3];
	buf[0]=(uint8_t)command;
	//Wire.beginTransmission(_address);
	//Wire.write(command);
	er_i2c=HAL_I2C_Master_Transmit(&hi2c1,OPT3001_address<<1,buf,1,1000);
	if(er_i2c!= HAL_OK){
			err=TIMEOUT_ERROR ;
    }
	return (err);
}

OPT3001_ErrorCode OPT3001_readData(uint16_t* data)
{
	uint8_t	buf[2];

//  Wire.requestFrom(_address, (uint8_t)2);
//	int counter = 0;
//	while (Wire.available() < 2)
//	{
//		counter++;
//		delay(10);
//		if (counter > 250)
//			return TIMEOUT_ERROR;
//	}
//
//	Wire.readBytes(buf, 2);
	HAL_StatusTypeDef er_i2c;
	uint16_t adr_rd=(OPT3001_address<<1);
	adr_rd =(adr_rd|0x0001);
	er_i2c=HAL_I2C_Master_Receive(&hi2c1,adr_rd,buf,2,1000);
	if(er_i2c!= HAL_OK){
		return(TIMEOUT_ERROR);
	}else{
	*data = (buf[0] << 8) | buf[1];
    }
	return NO_ERROR;

}


OPT3001 OPT3001_returnError(OPT3001_ErrorCode error) {
	OPT3001 result;
	result.lux = 0;
	result.error = error;
	return result;
}
