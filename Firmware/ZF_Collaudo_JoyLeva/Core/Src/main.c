/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "i2c.h"
#include "iwdg.h"
#include "rtc.h"
#include "tim.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "globals.h"
#include "display.h"
#include "stm32f0xx_it.h"
#include "keyboard_manager.h"
#include "PCA9955.h"

#include "usbd_cdc_if.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
//char msg[100];
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
  MX_GPIO_CUSTOM_Init();
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_I2C2_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM6_Init();
  MX_TIM7_Init();
  //MX_USART2_UART_Init();
  MX_USB_DEVICE_Init();
  //MX_IWDG_Init();
  MX_RTC_Init();
  MX_USART4_UART_Init();
  /* USER CODE BEGIN 2 */

  System_Clock_Init();
  HAL_TIM_Base_MspInit(&htim7);
  HAL_TIM_Base_Start_IT(&htim7);
  HAL_UART_MspInit(&huart4);
  HAL_GPIO_WritePin(LED1_DBG_GPIO_Port, LED1_DBG_Pin,GPIO_PIN_RESET);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  HAL_I2C_MspInit(&hi2c1);
  HAL_I2C_MspInit(&hi2c2);
  RxLed_Init();
  PCA9955_init();
  OPT3001_init();

  machine_status.b.POR=1;//reset
  machine_status.b.STOP0_RUN1=0;
  //sprintf(msg,"Test EEPROM \n");
  test_type=ZF_JOYSTICK_TEST;//ZF_LEVA_TEST;
  frame_rate_tx=1;
  tx_counter=frame_rate_tx;
  Read_sensor();
  sensor.b.UP_OLD=sensor.b.HALL_UP;
  machine_status.b.STOP0_RUN1=1;
  disp_init();
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  // RxLed_Process();
	         if (main_clock.flag_10ms==1){
	             main_clock.flag_10ms=0;
	             //Read_sensor();
	         }

	         if (main_clock.flag_50ms==1){
	        	 //if(machine_status.b.STOP0_RUN1==1){
	                KeyboardScanRead();
	        	 //}
	             //Tx_Keys_ZF();//Tx_Keys();
	             //SetLeds();

	        	 //HAL_GPIO_WritePin(LED1_DBG_GPIO_Port, LED1_DBG_Pin, HAL_GPIO_ReadPin(HALL_SENSOR1_GPIO_Port, HALL_SENSOR1_Pin));
	        	 //HAL_GPIO_WritePin(LED1_DBG_GPIO_Port, LED1_DBG_Pin,HAL_GPIO_ReadPin(LEVA_JOYSTICK_SEL_GPIO_Port, LEVA_JOYSTICK_SEL_Pin));
	             main_clock.flag_50ms=0;
	         }

	         if (main_clock.flag_100ms==1){

                 SetLedsDisplay();
	             main_clock.flag_100ms=0;
	         }

	         if (main_clock.flag_1sec==1){
	          HAL_GPIO_TogglePin(LED1_DBG_GPIO_Port, LED1_DBG_Pin);
	          ReadVarDisplay();
	          // Tx_Counter_ZF();
	          //if(opt3001_rd_time==1){
	         		opt3001_sensor=OPT3001_readResult();
	         		//PCA9955_AmbientLight_reg();
	         		//amblight_lux=(uint32_t)opt3001_sensor.lux;
	         		//printf("Amb.Light:%ld [lux]\n\r",(uint32_t)opt3001_sensor.lux);//to have lux value
	         		disp_data(LCM1.vp_var[LIGHT_SENSOR_VAR],opt3001_sensor.raw.rawData,2,0);
	         		opt3001_rd_time=0;
	           //}
	           main_clock.flag_1sec=0;
	         }

	 //        if (main_clock.flag_10sec==1){
	 //          main_clock.flag_10sec=0;
	 //        }

	 //        if (main_clock.flag_1min==1){
	 //            main_clock.flag_1min=0;
	 //        }

	         if(system_flag.b.b0==1){//some key pressed
//	        	if(tx_counter>1){
//	        		tx_counter--;
//	        	}else{
//	        	      tx_counter=frame_rate_tx;
//	        	      Tx_Counter_ZF();
//	        	     }
//	        	display_update_data();
//	        	sensor.b.HALL_UP=0;
//	        	sensor.b.UP_OLD=0;
                system_flag.b.b0=0;
	          }

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSI48
                              |RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI48;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB|RCC_PERIPHCLK_USART2
                              |RCC_PERIPHCLK_I2C1|RCC_PERIPHCLK_RTC;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
