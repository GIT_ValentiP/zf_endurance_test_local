/*
 ******************************************************************************
 *  @file      : display.c
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 14 apr 2021
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : NUOVA_BN_RS485_CAN 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */


/* Includes ----------------------------------------------------------*/
#include "display.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* Private typedef -----------------------------------------------------------*/
#include "usbd_cdc_if.h"
/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private user code ---------------------------------------------------------*/
void disp_init(){

		//char *buf = malloc(20*sizeof(char));
//        LCM1.vp_txt[TOT_TXT]=0x1100;
//		LCM1.vp_txt[TASTO1_TXT]=0x1120;
//		LCM1.vp_txt[TASTO2_TXT]=0x1140;
//		LCM1.vp_txt[TASTO3_TXT]=0x1160;
//		LCM1.vp_txt[COUNTS_TXT]=0x1200;
//		LCM1.vp_txt[ID_TXT]=0x1220;
//		LCM1.vp_txt[STATUS_TXT]=0x1260;



		LCM1.vp_var[KEYB_TYPE_VAR]=0x1010;//operation code CMD
		LCM1.vp_var[TASTO1_LEVA_ICO_VAR]=0x1100;//postazione
		LCM1.vp_var[TASTO2_LEVA_ICO_VAR]=0x1110;//postazione
		LCM1.vp_var[TASTO3_LEVA_ICO_VAR]=0x1120;//allarme
		LCM1.vp_var[TASTO4_LEVA_ICO_VAR]=0x1130;//
		LCM1.vp_var[TASTO5_LEVA_ICO_VAR]=0x1140;//operation code CMD
		LCM1.vp_var[TASTO6_LEVA_ICO_VAR]=0x1150;//postazione
		LCM1.vp_var[TASTO7_LEVA_ICO_VAR]=0x1160;//postazione
		LCM1.vp_var[TASTO8_LEVA_ICO_VAR]=0x1170;//allarme
		LCM1.vp_var[TASTO9_LEVA_ICO_VAR]=0x1180;//
		LCM1.vp_var[TASTO10_LEVA_ICO_VAR]=0x1190;//operation code CMD
		LCM1.vp_var[TASTO11_LEVA_ICO_VAR]=0x11A0;//postazione
		LCM1.vp_var[TASTO12_LEVA_ICO_VAR]=0x11B0;//postazione
		LCM1.vp_var[TASTO1_JOY_ICO_VAR]=0x1600;//allarme
		LCM1.vp_var[TASTO2_JOY_ICO_VAR]=0x1610;//
		LCM1.vp_var[TASTO3_JOY_ICO_VAR]=0x1620;//operation code CMD
		LCM1.vp_var[TASTO4_JOY_ICO_VAR]=0x1630;//postazione
//		LCM1.vp_var[TASTO5_JOY_ICO_VAR]=0x1640;//postazione
//		LCM1.vp_var[TASTO6_JOY_ICO_VAR]=0x1060;//allarme
//		LCM1.vp_var[TASTO7_JOY_ICO_VAR]=0x1060;//allarme
//		LCM1.vp_var[TASTO8_JOY_ICO_VAR]=0x1000;//
//		LCM1.vp_var[TASTO9_JOY_ICO_VAR]=0x1010;//operation code CMD
//		LCM1.vp_var[TASTO10_JOY_ICO_VAR]=0x1020;//postazione
//		LCM1.vp_var[TASTO11_JOY_ICO_VAR]=0x1040;//postazione
//		LCM1.vp_var[TASTO12_JOY_ICO_VAR]=0x1060;//allarme
		LCM1.vp_var[TASTO_LED_PRESS_VAR]=0x1500;//
		LCM1.vp_var[LIGHT_SENSOR_VAR]=0x1780;//
		LCM1.vp_var[LED_BRIGHT_VAR]=0x1680;//

		//LCM1.vp[0]=0x1000;//ico joy/leva
		//LCM1.vp[1]=0x1160;//ico pulsante piu


		disp_pic(9);
//		strcpy(LCM1.txt,"RES ");
//		disp_txt(LCM1.vp_txt[STATUS_TXT],LCM1.menu_id,LCM1.txt,4);
//		strcpy(LCM1.txt,"NC");
//		disp_txt(LCM1.vp_txt[TASTO1_TXT],LCM1.menu_id,LCM1.txt,2);
//		strcpy(LCM1.txt,"NC");
//		disp_txt(LCM1.vp_txt[TASTO2_TXT],LCM1.menu_id,LCM1.txt,2);
//		strcpy(LCM1.txt,"NC");
//		disp_txt(LCM1.vp_txt[TASTO3_TXT],LCM1.menu_id,LCM1.txt,2);
//
//		disp_data(LCM1.vp_var[COUNT_TOT_VAR],count_press_ZF[NUM_MAX_KEY_ZF-1].tot,4,0);
//		disp_data(LCM1.vp_var[COUNT_1_VAR],0,4,0);
//		disp_data(LCM1.vp_var[COUNT_2_VAR],0,4,0);
//		disp_data(LCM1.vp_var[COUNT_3_VAR],0,4,0);
//		//disp_data(LCM1.vp_var[KEYB_TYPE_VAR],test_type,1,0);
//		disp_ico(LCM1.vp_var[KEYB_TYPE_VAR],test_type,test_type);



		LCM1.menu_id=9;
		disp_pic(LCM1.menu_id);

}





void disp_pic(uint16_t id){
uint8_t len,len_data;
uint8_t TX[10];

len_data=0x07;
len=0;
TX[len++]=0x5A;
TX[len++]=0xA5;
TX[len++]=len_data;
TX[len++]=0x82;//LCM_CMD_WR
TX[len++]=0x00;
TX[len++]=0x84;
TX[len++]=0x5A;
TX[len++]=0x01;
TX[len++]=(id>>8);
TX[len++]=(id & 0x00FF);

HAL_UART_Transmit(&huart4, (uint8_t *)&TX, len, 0xFFFF);
}



void disp_txt(uint16_t vp,uint16_t pic_id,char *txt,uint8_t length){
	uint8_t len,len_data,i;
	uint8_t TX[30];
	len_data=length+0x03;
	len=0;
	TX[len++]=0x5A;
	TX[len++]=0xA5;
	TX[len++]=len_data;
	TX[len++]=0x82;//LCM_CMD_WR
	TX[len++]=(vp>>8);
	TX[len++]=(vp & 0x00FF);
	for(i=0;i<length;i++){
		TX[len++]=txt[i];
	}
	HAL_UART_Transmit(&huart4, (uint8_t *)&TX, len, 0xFFFF);

}






void disp_ico(uint16_t vp,uint16_t pic_id,uint16_t val){
	uint8_t len,len_data;
	uint8_t TX[10];
	len_data=0x05;
	len=0;
	TX[len++]=0x5A;
	TX[len++]=0xA5;
	TX[len++]=len_data;
	TX[len++]=0x82;//LCM_CMD_WR
	TX[len++]=(vp>>8);
	TX[len++]=(vp & 0x00FF);
	TX[len++]=0x00;
	TX[len++]=(val & 0x00FF);
	HAL_UART_Transmit(&huart4, (uint8_t *)&TX, len, 0xFFFF);


}





void disp_data(uint16_t vp,uint32_t val,uint8_t len_int,uint8_t len_dec){
	    uint8_t len,len_data;
		uint8_t TX[10];
		count_t value;
		value.tot=val;
		len_data=(0x03+len_int);
		len=0;
		TX[len++]=0x5A;
		TX[len++]=0xA5;
		TX[len++]=len_data;
		TX[len++]=0x82;//LCM_CMD_WR
		TX[len++]=(vp>>8);
		TX[len++]=(vp & 0x00FF);
		if(len_int>2){
		   TX[len++]=(value.b.HH4);
		   TX[len++]=(value.b.HL3);
		}
		TX[len++]=(value.b.LH2);
		TX[len++]=(value.b.LL1);
		HAL_UART_Transmit(&huart4, (uint8_t *)&TX, len, 0xFFFF);

}


uint8_t disp_read_data(uint16_t vp,uint8_t *rd,uint8_t len_int){
	    uint16_t len,len_RX;
		uint8_t len_data;
		uint8_t TX[10];

		uint16_t i;
		//count_t value;
		HAL_StatusTypeDef result;
		//5A A5 04 83 1500 01
		//value.tot=val;
		len_data=(0x03+len_int);
		len=0;
		TX[len++]=0x5A;
		TX[len++]=0xA5;
		TX[len++]=len_data;
		TX[len++]=0x83;//LCM_CMD_RD
		TX[len++]=(vp>>8);
		TX[len++]=(vp & 0x00FF);
		TX[len++]=len_int;
		//CDC_Transmit_FS(TX, len);//

	    for (i=0;i<10;i++){LCM1.RX[i]=0;}   //clear Rx_Buffer before receiving new data
		len_RX=9;
		LCM1.RX[7]=0xFF;
		LCM1.Transfer_cplt=0;

		HAL_UART_Transmit(&huart4, (uint8_t *)&TX, len, 0xFFFF);
        result=HAL_UART_Receive_IT(&huart4,(uint8_t *)&LCM1.RX,len_RX);// //activate UART receive interrupt every time
        i=0;
		while(i<10000){
        	i++;
        	if(LCM1.Transfer_cplt==1){
        		i=10000;

        	}
        }
        result=1;
		//if(LCM1.Transfer_cplt==1){//receive ok((result==HAL_OK)&&
			if(LCM1.RX[2]==0x06){
			    LCM1.RX[3]=0x82;//LCM_CMD_RD
			    CDC_Transmit_FS(LCM1.RX,9);//

				rd[0]=LCM1.RX[8];//Low byte
			    rd[1]=LCM1.RX[7];//high byte
			    result=0;
			}
		//}
		return(result);

}




void disp_rtc(uint16_t vp,uint16_t year,uint8_t month,uint8_t day,uint8_t hour,uint8_t min){





}




void disp_set_rtc(uint16_t vp,uint16_t year,uint8_t month,uint8_t day,uint8_t hour,uint8_t min){






}

void display_update_txt(void){
uint8_t i;
uint32_t val;

strcpy(LCM1.txt,"NC");
disp_txt(LCM1.vp_txt[TASTO1_TXT],LCM1.menu_id,LCM1.txt,2);
strcpy(LCM1.txt,"NC");
disp_txt(LCM1.vp_txt[TASTO2_TXT],LCM1.menu_id,LCM1.txt,2);
strcpy(LCM1.txt,"NC");
disp_txt(LCM1.vp_txt[TASTO3_TXT],LCM1.menu_id,LCM1.txt,2);

for(i=0;i<NMAX_KEYPRESSED_TEST;i++){
      val=0;
	  if(config_id_key[i] < (NUM_MAX_KEY_ZF-1)){

			sprintf(LCM1.txt,"%.2d",config_id_key[i]);
			val=count_press_ZF[config_id_key[i]].tot;

	  }
	  switch(i){
	 		  	     case(0):
		                     disp_txt(LCM1.vp_txt[TASTO1_TXT],LCM1.menu_id,LCM1.txt,2);
	 		  	             disp_data(LCM1.vp_var[COUNT_1_VAR],val,4,0);
	 		  	    		 break;
	 		  	     case(1):
		                     disp_txt(LCM1.vp_txt[TASTO2_TXT],LCM1.menu_id,LCM1.txt,2);
	 		  	             disp_data(LCM1.vp_var[COUNT_2_VAR],val,4,0);
	 		  	    	     break;
	 		  	     case(2):
		                     disp_txt(LCM1.vp_txt[TASTO3_TXT],LCM1.menu_id,LCM1.txt,2);
	 		  	             disp_data(LCM1.vp_var[COUNT_3_VAR],val,4,0);
	 		  	    	     break;

	 		  	  }

  }
  disp_data(LCM1.vp_var[COUNT_TOT_VAR],count_press_ZF[NUM_MAX_KEY_ZF-1].tot,4,0);

}


void display_update_data(void){
uint8_t i;
uint32_t val;

disp_data(LCM1.vp_var[KEYB_TYPE_VAR],test_type,1,0);

for(i=0;i<NMAX_KEYPRESSED_TEST;i++){

      val=0;
	  if(config_id_key[i] < (NUM_MAX_KEY_ZF-1)){
         //Send_count_answer(config_id_key[i]);
		  val=count_press_ZF[config_id_key[i]].tot;
	  }
	  switch(i){
	 		  	     case(0):
	 		  				 disp_data(LCM1.vp_var[COUNT_1_VAR],val,4,0);
	 		  	    		 break;
	 		  	     case(1):
	 		  	    		 disp_data(LCM1.vp_var[COUNT_2_VAR],val,4,0);
	 		  	    	     break;
	 		  	     case(2):
	 		  	    		 disp_data(LCM1.vp_var[COUNT_3_VAR],val,4,0);
	 		  	    	     break;

	 		  	  }

  }
  disp_data(LCM1.vp_var[COUNT_TOT_VAR],count_press_ZF[NUM_MAX_KEY_ZF-1].tot,4,0);

}

void display_update_ico(uint8_t scan_code,uint16_t val)
{
	uint16_t vp;
	//uint16_t val;
	//uint8_t i;

	/*ZF LEVA and JOYSTICK*/
	if(test_type==ZF_LEVA_TEST){//LEVA
			switch(scan_code)
			{
			   case(1)://(KEY_TRIMPORT_M_1):
										  vp=LCM1.vp_var[TASTO9_LEVA_ICO_VAR];
										  break;


			   case(2)://(KEY_TRIMPORT_P_1):
										  vp=LCM1.vp_var[TASTO10_LEVA_ICO_VAR];
										  break;


			   case(3)://(KEY_TRIMSTBD_M_1):
										  vp=LCM1.vp_var[TASTO11_LEVA_ICO_VAR] ;
										  break;

			   case(4)://(KEY_TRIMSTBD_P_1):
										 vp=LCM1.vp_var[TASTO12_LEVA_ICO_VAR];
										  break;


			   case(5)://(KEY_INCDEC_1):
										 vp=LCM1.vp_var[TASTO1_LEVA_ICO_VAR] ;
										  break;


			   case(6)://(KEY_PIU_1):
										 vp=LCM1.vp_var[TASTO2_LEVA_ICO_VAR] ;
										  break;


			   case(7)://(KEY_CTRL23_1):
										  vp=LCM1.vp_var[TASTO8_LEVA_ICO_VAR] ;
										  break;

			   case(8)://(KEY_WARMUP_1):
										  vp=LCM1.vp_var[TASTO5_LEVA_ICO_VAR] ;
										  break;


				case(9)://(KEY_TROLL_1):
										  vp=LCM1.vp_var[TASTO7_LEVA_ICO_VAR] ;
										  break;


				case(10)://(KEY_MENO_1):
										  vp=LCM1.vp_var[TASTO6_LEVA_ICO_VAR] ;
										  break;


				case(11)://(KEY_CTRL1_1):
										  vp=LCM1.vp_var[TASTO4_LEVA_ICO_VAR] ;
										  break;


				case(12)://(KEY_ONELEVER_1):
										  vp=LCM1.vp_var[TASTO3_LEVA_ICO_VAR] ;
										  break;




			   default:
				       vp=0;

					   break;
			}

		   if(vp>0)
		   {
			disp_ico(vp,val,val);
		   }
//		   else{
//			       vp=LCM1.vp_var[TASTO1_LEVA_ICO_VAR] ;
//			       for(i=0;i<11;i++)
//			       {
//			    	   disp_ico(vp,val,val);
//			    	   vp+=0x0010;
//			       }
//		        }

	}else {//joystick

		switch(scan_code)
				{
				   case(1)://(KEY_JOY_BOOST_1):
											  vp=LCM1.vp_var[TASTO2_JOY_ICO_VAR];
											  break;


				   case(2)://(KEY_JOY_CTRL123_1):
											  vp=LCM1.vp_var[TASTO4_JOY_ICO_VAR];
											  break;


				   case(3)://(KEY_JOY_IANCHOR_1):
											  vp=LCM1.vp_var[TASTO1_JOY_ICO_VAR] ;
											  break;

				   case(4)://(KEY_JOY_THRUSTER_1):
											 vp=LCM1.vp_var[TASTO3_JOY_ICO_VAR];
											  break;


				   default:
					       vp=0;

						   break;
				}

			   if(vp>0)
			   {
				disp_ico(vp,val,val);
			   }
	      }
	LCM1.RX[0]=0x5A;
	LCM1.RX[1]=scan_code;
	LCM1.RX[2]=val;
	 //CDC_Transmit_FS(LCM1.RX,3);//
}
