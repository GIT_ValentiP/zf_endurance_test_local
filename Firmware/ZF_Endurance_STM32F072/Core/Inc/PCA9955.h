/*
 *****************************************************************************
 *  @file      : PCA9955.h
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 30 mag 2020
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : Sanremo_REVO3 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

#ifndef INC_PCA9955_H_
#define INC_PCA9955_H_
/* Includes ----------------------------------------------------------*/
#include "main.h"
#include "globals.h"

/* Typedef -----------------------------------------------------------*/

/* Define ------------------------------------------------------------*/


/* Macro -------------------------------------------------------------*/


/* Variables ---------------------------------------------------------*/



/* Function prototypes -----------------------------------------------*/
/** PCA9952/55 class.
 *  Used for controlling a PCA9952/55 constant current LED driver connected via I2C.
 *
 * Example:
 * @code
 * #include "mbed.h"
 * #include "PCA9955.h"
 *
 * //Create a PCA9955 object at the default address (ADDRESS_0)
 * PCA9955 driver(p28, p27);
 *
 * int main()
 * {
 *     //Try to open the PCA9955
 *     if (driver.open()) {
 *         printf("Device detected!\n");
 *
 *         //Reset the device
 *         //NOTE: This might reset other I2C devices as well!
 *         driver.reset();
 *         wait_ms(5);
 *
 *         //Set all of the output states to PWM mode
 *         driver.allOutputStates(PCA9955::OUTPUT_PWM);
 *
 *         //Set all of the output currents to maximum
 *         driver.allOutputCurrents(1.0);
 *
 *         while (1) {
 *             //Generate a breathing effect on all of the outputs
 *             for (float i = 0.0; i < 360.0; i += 0.1)
 *                 driver = 0.5 * (sinf(i * 3.14159265 / 180.0) + 1);
 *         }
 *     } else {
 *         error("Device not detected!\n");
 *     }
 * }
 * @endcode
 */

    /** Represents the different I2C address possibilities for the PCA9952/55
     */
  typedef enum  {
        ADDRESS_0 = (0x60 << 1),    /**< A[3:0] pins = 0000 */
        ADDRESS_1 = (0x61 << 1),    /**< A[3:0] pins = 0001 */
        ADDRESS_2 = (0x62 << 1),    /**< A[3:0] pins = 0010 */
        ADDRESS_3 = (0x63 << 1),    /**< A[3:0] pins = 0011 */
        ADDRESS_4 = (0x64 << 1),    /**< A[3:0] pins = 0100 */
        ADDRESS_5 = (0x65 << 1),    /**< A[3:0] pins = 0101 */
        ADDRESS_6 = (0x66 << 1),    /**< A[3:0] pins = 0110 */
        ADDRESS_7 = (0x67 << 1),    /**< A[3:0] pins = 0111 */
        ADDRESS_8 = (0x68 << 1),    /**< A[3:0] pins = 1000 (not available on PCA9952) */
        ADDRESS_9 = (0x69 << 1),    /**< A[3:0] pins = 1001 (not available on PCA9952) */
        ADDRESS_10 = (0x6A << 1),   /**< A[3:0] pins = 1010 (not available on PCA9952) */
        ADDRESS_11 = (0x6B << 1),   /**< A[3:0] pins = 1011 (not available on PCA9952) */
        ADDRESS_12 = (0x6C << 1),   /**< A[3:0] pins = 1100 (not available on PCA9952) */
        ADDRESS_13 = (0x6D << 1),   /**< A[3:0] pins = 1101 (not available on PCA9952) */
        ADDRESS_14 = (0x6E << 1),   /**< A[3:0] pins = 1110 (not available on PCA9952) */
        ADDRESS_15 = (0x6F << 1)    /**< A[3:0] pins = 1111 (not available on PCA9952) */
    }Address;

    /** Represents the different outputs of the PCA9952/55
     */
  typedef enum  {
        OUTPUT_0 = 0,   /**< LED output 0 */
        OUTPUT_1 = 1,   /**< LED output 1 */
        OUTPUT_2 = 2,   /**< LED output 2 */
        OUTPUT_3 = 3,   /**< LED output 3 */
        OUTPUT_4 = 4,   /**< LED output 4 */
        OUTPUT_5 = 5,   /**< LED output 5 */
        OUTPUT_6 = 6,   /**< LED output 6 */
        OUTPUT_7 = 7,   /**< LED output 7 */
        OUTPUT_8 = 8,   /**< LED output 8 */
        OUTPUT_9 = 9,   /**< LED output 9 */
        OUTPUT_10 = 10, /**< LED output 10 */
        OUTPUT_11 = 11, /**< LED output 11 */
        OUTPUT_12 = 12, /**< LED output 12 */
        OUTPUT_13 = 13, /**< LED output 13 */
        OUTPUT_14 = 14, /**< LED output 14 */
        OUTPUT_15 = 15  /**< LED output 15 */
    }Output;

    /** Represents the power mode of the PCA9952/55
     */
  typedef enum  {
        POWER_NORMAL,   /**< Oscillator is enabled */
        POWER_SHUTDOWN  /**< Oscillator is disabled */
    }PowerMode;

    /** Represents the output change mode of the PCA9952/55
     */
  typedef enum  {
        OUTPUT_CHANGE_ON_STOP,  /**< Outputs change on STOP command */
        OUTPUT_CHANGE_ON_ACK    /**< Outputs change on ACK */
    }OutputChangeMode;

    /** Represents the group control mode of the PCA9952/55
     */
  typedef enum  {
        GROUP_DIMMING,  /**< Group control = dimming */
        GROUP_BLINKING  /**< group control = blinking */
    }GroupMode;

    /** Represents the individual driver output states of the PCA9952/55
     */
  typedef enum  {
        OUTPUT_OFF,         /**< LED driver x is off (default power-up state) */
        OUTPUT_ON,          /**< LED driver x is fully on (individual brightness and group dimming/blinking not controlled) */
        OUTPUT_PWM,         /**< LED driver x individual brightness can be controlled through its PWMx register */
        OUTPUT_PWM_GRPPWM   /**< LED driver x individual brightness and group dimming/blinking can be controlled through its PWMx register and the GRPPWM registers */
    }OutputState;

    /** Represents the fault test flags for the PCA9952/55
     */
  typedef enum  {
        FAULT_OUTPUT_0 = (1 << 0),      /**< LED output 0 is either open or shorted */
        FAULT_OUTPUT_1 = (1 << 1),      /**< LED output 1 is either open or shorted */
        FAULT_OUTPUT_2 = (1 << 2),      /**< LED output 2 is either open or shorted */
        FAULT_OUTPUT_3 = (1 << 3),      /**< LED output 3 is either open or shorted */
        FAULT_OUTPUT_4 = (1 << 4),      /**< LED output 4 is either open or shorted */
        FAULT_OUTPUT_5 = (1 << 5),      /**< LED output 5 is either open or shorted */
        FAULT_OUTPUT_6 = (1 << 6),      /**< LED output 6 is either open or shorted */
        FAULT_OUTPUT_7 = (1 << 7),      /**< LED output 7 is either open or shorted */
        FAULT_OUTPUT_8 = (1 << 8),      /**< LED output 8 is either open or shorted */
        FAULT_OUTPUT_9 = (1 << 9),      /**< LED output 9 is either open or shorted */
        FAULT_OUTPUT_10 = (1 << 10),    /**< LED output 10 is either open or shorted */
        FAULT_OUTPUT_11 = (1 << 11),    /**< LED output 11 is either open or shorted */
        FAULT_OUTPUT_12 = (1 << 12),    /**< LED output 12 is either open or shorted */
        FAULT_OUTPUT_13 = (1 << 13),    /**< LED output 13 is either open or shorted */
        FAULT_OUTPUT_14 = (1 << 14),    /**< LED output 14 is either open or shorted */
        FAULT_OUTPUT_15 = (1 << 15)     /**< LED output 15 is either open or shorted */
    }FaultFlags;



    //I2C register addresses
  typedef enum  {
        REG_MODE1       = 0x00,
        REG_MODE2       = 0x01,
        REG_LEDOUT0     = 0x02,
        REG_LEDOUT1     = 0x03,
        REG_LEDOUT2     = 0x04,
        REG_LEDOUT3     = 0x05,
        REG_GRPPWM      = 0x06,
        REG_GRPFREQ     = 0x07,
        REG_PWM0        = 0x08,
        REG_PWM1        = 0x09,
        REG_PWM2        = 0x0A,
        REG_PWM3        = 0x0B,
        REG_PWM4        = 0x0C,
        REG_PWM5        = 0x0D,
        REG_PWM6        = 0x0E,
        REG_PWM7        = 0x0F,
        REG_PWM8        = 0x10,
        REG_PWM9        = 0x11,
        REG_PWM10       = 0x12,
        REG_PWM11       = 0x13,
        REG_PWM12       = 0x14,
        REG_PWM13       = 0x15,
        REG_PWM14       = 0x16,
        REG_PWM15       = 0x17,
        REG_IREF0       = 0x18,
        REG_IREF1       = 0x19,
        REG_IREF2       = 0x1A,
        REG_IREF3       = 0x1B,
        REG_IREF4       = 0x1C,
        REG_IREF5       = 0x1D,
        REG_IREF6       = 0x1E,
        REG_IREF7       = 0x1F,
        REG_IREF8       = 0x20,
        REG_IREF9       = 0x21,
        REG_IREF10      = 0x22,
        REG_IREF11      = 0x23,
        REG_IREF12      = 0x24,
        REG_IREF13      = 0x25,
        REG_IREF14      = 0x26,
        REG_IREF15      = 0x27,
		REG_RAMP_GRP0   = 0x28,
		REG_STEP_GRP0   = 0x29,
		REG_HOLD_GRP0   = 0x2A,
		REG_IREF_GRP0   = 0x2B,
		REG_RAMP_GRP1   = 0x2C,
		REG_STEP_GRP1   = 0x2D,
		REG_HOLD_GRP1   = 0x2E,
		REG_IREF_GRP1   = 0x2F,
		REG_RAMP_GRP2   = 0x30,
		REG_STEP_GRP2   = 0x31,
		REG_HOLD_GRP2   = 0x32,
		REG_IREF_GRP2   = 0x33,
		REG_RAMP_GRP3   = 0x34,
		REG_STEP_GRP3   = 0x35,
		REG_HOLD_GRP3   = 0x36,
		REG_IREF_GRP3   = 0x37,
		REG_GRAD_SEL0   = 0x38,
		REG_GRAD_SEL1   = 0x39,
		REG_GRAD_GRP_SEL0= 0x3A,
		REG_GRAD_GRP_SEL1= 0x3B,
		REG_GRAD_GRP_SEL2= 0x3C,
		REG_GRAD_GRP_SEL3= 0x3D,
		REG_GRAD_CNTL   = 0x3E,
        REG_OFFSET      = 0x3F,
        REG_SUBADR1     = 0x40,
        REG_SUBADR2     = 0x41,
        REG_SUBADR3     = 0x42,
        REG_ALLCALLADR  = 0x43,
        REG_PWMALL      = 0x44,
        REG_IREFALL     = 0x45,
        REG_EFLAG0      = 0x46,
        REG_EFLAG1      = 0x47,
		REG_EFLAG2      = 0x48,
		REG_EFLAG3      = 0x49,
		REG_RESERVED1   = 0x4A,
        REG_RESERVED2   = 0x4B,
        REG_RESERVED3   = 0x4C,
        REG_AUTO_INC    = 0x80
    }Register;

    /** The default LED All Call address
     */
    static const int ALLCALL_ADDRESS = (0x70 << 1);

    /** The default subaddress for 16-channel LED drivers
     */
    static const int SUBCALL_ADDRESS = (0x76 << 1);

    /** Create a PCA9952/55 object connected to the specified I2C pins with the specified I2C slave address
     *
     * @param sda The I2C data pin.
     * @param scl The I2C clock pin.
     * @param addr The I2C slave address (defaults to ADDRESS_0).
     * @param hz The I2C bus frequency (defaults to 400kHz).
     */
   void PCA9955_init(void);

    /** Probe for the PCA9952/55 and indicate if it's present on the bus
     *
     * @returns
     *   'true' if the device exists on the bus,
     *   'false' if the device doesn't exist on the bus.
     */
    bool PCA9955_open();

    /** Issue a General Call Reset command to reset all PCA9952/55 devices on the bus
     *
     * @note Requires a 5ms delay before issuing additional commands
     *
     * @warning This might reset other I2C devices as well
     */
    void PCA9955_reset();

    /** Determine whether the LED All Call address is enabled on the PCA9952/55
     *
     * @returns Whether or not the LED All Call address is enabled.
     */
    bool PCA9955_getallCallEnabled();

    /** Set whether the LED All Call address is enabled on the PCA9952/55
     *
     * @param enabled Whether or not the LED All Call address is enabled.
     */
    void PCA9955_setallCallEnabled(bool enabled);

    /** Determine whether subaddress 3 is enabled on the PCA9952/55
     *
     * @returns Whether or not subaddress 3 is enabled.
     */
    bool PCA9955_getsubCall3Enabled();

    /** Set whether subaddress 3 is enabled on the PCA9952/55
     *
     * @param enabled Whether or not subaddress 3 is enabled.
     */
    void PCA9955_setsubCall3Enabled(bool enabled);

    /** Determine whether subaddress 2 is enabled on the PCA9952/55
     *
     * @returns Whether or not subaddress 2 is enabled.
     */
    bool PCA9955_getsubCall2Enabled();

    /** Set whether subaddress 2 is enabled on the PCA9952/55
     *
     * @param enabled Whether or not subaddress 2 is enabled.
     */
    void PCA9955_setsubCall2Enabled(bool enabled);

    /** Determine whether subaddress 1 is enabled on the PCA9952/55
     *
     * @returns Whether or not subaddress 1 is enabled.
     */
    bool PCA9955_getsubCall1Enabled();

    /** Set whether subaddress 1 is enabled on the PCA9952/55
     *
     * @param enabled Whether or not subaddress 1 is enabled.
     */
    void PCA9955_setsubCall1Enabled(bool enabled);

    /** Get the current power mode of the PCA9952/55
     *
     * @returns The current power mode as a PowerMode enum.
     */
    PowerMode PCA9955_getpowerMode();

    /** Set the power mode of the PCA9952/55
     *
     * @param mode The new power mode as a PowerMode enum.
     */
    void PCA9955_setpowerMode(PowerMode mode);

    /** Get the current output change mode of the PCA9952/55
     *
     * @returns The current output change mode as an OutputChangeMode enum.
     */
    OutputChangeMode PCA9955_getoutputChangeMode();

    /** Set the output change mode of the PCA9952/55
     *
     * @param mode The new output change mode as an OutputChangeMode enum.
     */
    void PCA9955_setoutputChangeMode(OutputChangeMode mode);

    /** Get the current group control mode of the PCA9952/55
     *
     * @returns The current group control mode as a GroupMode enum.
     */
    GroupMode PCA9955_getgroupMode();

    /** Set the group control mode of the PCA9952/55
     *
     * @param mode The new group control mode as a GroupMode enum.
     */
    void PCA9955_setgroupMode(GroupMode mode);

    /** Determine whether or not the PCA9952/55 is overheating
     *
     * @returns
     *   'true' if the device is currently disabled due to overheating,
     *   'false' if the device is functioning normally.
     */
    bool PCA9955_overTemp();

    /** Get the specified output's state
     *
     * @param output The output to check.
     *
     * @returns The output's current state as an OutputState enum.
     */
    OutputState PCA9955_getoutputState(Output output);

    /** Set the specified output's state
     *
     * @param output The output to change.
     * @param state The new output state as an OutputState enum.
     */
    void PCA9955_setoutputState(Output output, OutputState state);

    /** Get the current group control duty cycle of the PCA9952/55 in percent
     *
     * @returns The current group control duty cycle as a float (0.0 to 1.0).
     */
    float PCA9955_getgroupDuty();

    /** Set the group control duty cycle of the PCA9952/55 in percent
     *
     * @param duty The new group control duty cycle as a float (0.0 to 1.0).
     * @param altAddr An optional, alternate address such as a subaddress or LED All Call address (defaults to NULL).
     */
    void PCA9955_setgroupDuty(float duty, int altAddr);

    /** Get the current group control duty cycle of the PCA9952/55
     *
     * @returns The current group control duty cycle as an unsigned char (0 to 255).
     */
    char PCA9955_groupDuty_u8();

    /** Set the group control duty cycle of the PCA9952/55
     *
     * @param duty The new group control duty cycle as an unsigned char (0 to 255).
     * @param altAddr An optional, alternate address such as a subaddress or LED All Call address (defaults to NULL).
     */
    void groupDuty_u8(char duty, int altAddr );

    /** Get the current group control blink period of the PCA9952/55 in seconds
     *
     * @returns The current group control blink period in seconds (0.067 to 16.8).
     */
    float PCA9955_getgroupBlinkPeriod();

    /** Set the current group control blink period of the PCA9952/55 in seconds
     *
     * @param period The new group control blink period in seconds (0.067 to 16.8).
     * @param altAddr An optional, alternate address such as a subaddress or LED All Call address (defaults to NULL).
     */
    void PCA9955_setgroupBlinkPeriod(float period, int altAddr);

    /** Get the current group control blink period of the PCA9952/55
     *
     * @returns The current group control blink period as an unsigned char (0 to 255).
     */
    char PCA9955_getgroupBlinkPeriod_u8();

    /** Set the current group control blink period of the PCA9952/55
     *
     * @param period The new group control blink period as an unsigned char (0 to 255).
     * @param altAddr An optional, alternate address such as a subaddress or LED All Call address (defaults to NULL).
     */
    void PCA9955_setgroupBlinkPeriod_u8(char period, int altAddr);

    /** Get the specified output's duty cycle in percent
     *
     * @param output The output to check.
     *
     * @returns The output's current duty cycle as a float (0.0 to 1.0).
     */
    float PCA9955_getoutputDuty(Output output);

    /** Set the specified output's duty cycle in percent
     *
     * @param output The output to change.
     * @param duty The new output duty cycle as a float (0.0 to 1.0).
     * @param altAddr An optional, alternate address such as a subaddress or LED All Call address (defaults to NULL).
     */
    void PCA9955_setoutputDuty(Output output, float duty, int altAddr);

    /** Get the specified output's duty cycle
     *
     * @param output The output to check.
     *
     * @returns The output's current duty cycle as an unsigned char (0 to 255).
     */
    char PCA9955_getoutputDuty_u8(Output output);

    /** Set the specified output's duty cycle
     *
     * @param output The output to change.
     * @param duty The new output duty cycle as an unsigned char (0 to 255).
     * @param altAddr An optional, alternate address such as a subaddress or LED All Call address (defaults to NULL).
     */
    void PCA9955_setoutputDuty_u8(Output output, char duty, int altAddr);

    /** Get the specified output's current reference in percent
     *
     * @param output The output to check.
     *
     * @returns The output's current reference as a float (0.0 to 1.0).
     */
    float PCA9955_getoutputCurrent(Output output);

    /** Set the specified output's current reference in percent
     *
     * @param output The output to change.
     * @param iref The new output current reference as a float (0.0 to 1.0).
     * @param altAddr An optional, alternate address such as a subaddress or LED All Call address (defaults to NULL).
     */
    void PCA9955_setoutputCurrent(Output output, float iref, int altAddr);

    /** Get the specified output's current reference
     *
     * @param output The output to check.
     *
     * @returns The output's current reference as an unsigned char (0 to 255).
     */
    char PCA9955_getoutputCurrent_u8(Output output);

    /** Set the specified output's current reference
     *
     * @param output The output to change.
     * @param iref The new output current reference as an unsigned char (0 to 255).
     * @param altAddr An optional, alternate address such as a subaddress or LED All Call address (defaults to NULL).
     */
    void PCA9955_setoutputCurrent_u8(Output output, char iref, int altAddr);

    /** Get the turn-on delay between LEDn outputs
     *
     * @returns The turn-on delay between LEDn outputs in clocks (0 to 15 - 125ns per clock).
     */
    char PCA9955_outputDelay();

    /** Set the specified output's current reference
     *
     * @param clocks The turn-on delay between LEDn outputs in clocks (0 to 15 - 125ns per clock).
     * @param altAddr An optional, alternate address such as a subaddress or LED All Call address (defaults to NULL).
     */
    void outputDelay(char clocks, int altAddr);

    /** Get subaddress 1
     *
     * @returns The current I2C subaddress 1.
     */
    char PCA9955_getsubCall1Addr();

    /** Set subaddress 1
     *
     * @param addr The new I2C subaddress 1.
     * @param altAddr An optional, alternate address such as a subaddress or LED All Call address (defaults to NULL).
     */
    void PCA9955_setsubCall1Addr(char addr, int altAddr);

    /** Get subaddress 2
     *
     * @returns The current I2C subaddress 2.
     */
    char PCA9955_getsubCall2Addr();

    /** Set subaddress 2
     *
     * @param addr The new I2C subaddress 2.
     * @param altAddr An optional, alternate address such as a subaddress or LED All Call address (defaults to NULL).
     */
    void PCA9955_setsubCall2Addr(char addr, int altAddr);

    /** Get subaddress 3
     *
     * @returns The current I2C subaddress 3.
     */
    char PCA9955_getsubCall3Addr();

    /** Set subaddress 3
     *
     * @param addr The new I2C subaddress 3.
     * @param altAddr An optional, alternate address such as a subaddress or LED All Call address (defaults to NULL).
     */
    void PCA9955_setsubCall3Addr(char addr, int altAddr);

    /** Get the LED All Call address
     *
     * @returns The current LED All Call address.
     */
    char PCA9955_getallCallAddr();

    /** Set the LED All Call address
     *
     * @param addr The new LED All Call address.
     * @param altAddr An optional, alternate address such as a subaddress or LED All Call address (defaults to NULL).
     */
    void PCA9955_setallCallAddr(char addr, int altAddr);

    /** Read a range of output states into an array
     *
     * @param states Pointer to an array for up to 16 output states.
     * @param start The first output to read (defaults to OUTPUT_0).
     * @param end The last output to read (defaults to OUTPUT_15).
     */
    void PCA9955_getOutputStates(OutputState* states, Output start, Output end);

    /** Set a range of output states from an array
     *
     * @param states Pointer to an array of up to 16 output states.
     * @param start The first output to set (defaults to OUTPUT_0).
     * @param end The last output to set (defaults to OUTPUT_15).
     */
    void PCA9955_setOutputStates(OutputState* states, Output start , Output end );

    /** Set all of the output states to the same state
     *
     * @param state The new output state for all outputs.
     * @param altAddr An optional, alternate address such as a subaddress or LED All Call address (defaults to NULL).
     */
    void PCA9955_setallOutputStates(OutputState state, int altAddr);

    /** Read a range of output duty cycles into an array as percents
     *
     * @param duties Pointer to an array for up to 16 duty cycles as floats (0.0 to 1.0).
     * @param start The first output to read (defaults to OUTPUT_0).
     * @param end The last output to read (defaults to OUTPUT_15).
     */
    void PCA9955_getOutputDuties(float* duties, Output start, Output end);

    /** Set a range of output duty cycles from an array of percents
     *
     * @param duties Pointer to an array of up to 16 duty cycles as floats (0.0 to 1.0).
     * @param start The first output to set (defaults to OUTPUT_0).
     * @param end The last output to set (defaults to OUTPUT_15).
     * @param altAddr An optional, alternate address such as a subaddress or LED All Call address (defaults to NULL).
     */
    void PCA9955_setOutputDuties(float* duties, Output start, Output end, int altAddr);

    /** Set all of the output duty cycles to the same value in percent
     *
     * @param duty The new duty cycle for all outputs as a float (0.0 to 1.0).
     * @param altAddr An optional, alternate address such as a subaddress or LED All Call address (defaults to NULL).
     */
    void PCA9955_setallOutputDuties(float duty, int altAddr);

    /** Read a range of output duty cycles into an array
     *
     * @param duties Pointer to an array for up to 16 duty cycles as unsigned chars (0 to 255).
     * @param start The first output to read (defaults to OUTPUT_0).
     * @param end The last output to read (defaults to OUTPUT_15).
     */
    void PCA9955_getOutputDuties_u8(char* duties, Output start , Output end );

    /** Set a range of output duty cycles from an array
     *
     * @param duties Pointer to any array of up to 16 duty cycles as unsigned chars (0 to 255).
     * @param start The first output to set (defaults to OUTPUT_0).
     * @param end The last output to set (defaults to OUTPUT_15).
     * @param altAddr An optional, alternate address such as a subaddress or LED All Call address (defaults to NULL).
     */
    void PCA9955_setOutputDuties_u8(char* duties, Output start, Output end , int altAddr );

    /** Set all of the output duty cycles to the same value
     *
     * @param duty The new duty cycle for all outputs as an unsigned char (0 to 255).
     * @param altAddr An optional, alternate address such as a subaddress or LED All Call address (defaults to NULL).
     */
    void PCA9955_allOutputDuties_u8(char duty, int altAddr);

    /** Read a range of output current references into an array as percents
     *
     * @param irefs Pointer to an array for up to 16 current references as floats (0.0 to 1.0).
     * @param start The first output to read (defaults to OUTPUT_0).
     * @param end The last output to read (defaults to OUTPUT_15).
     */
    void PCA9955_getOutputCurrents(float* irefs, Output start, Output end);

    /** Set a range of output current references from an array of percents
     *
     * @param irefs Pointer to an array of up to 16 current references as floats (0.0 to 1.0).
     * @param start The first output to set (defaults to OUTPUT_0).
     * @param end The last output to set (defaults to OUTPUT_15).
     * @param altAddr An optional, alternate address such as a subaddress or LED All Call address (defaults to NULL).
     */
    void PCA9955_setOutputCurrents(float* irefs, Output start, Output end , int altAddr );

    /** Set all of the output current references to the same value in percent
     *
     * @param iref The new current reference for all outputs as a float (0.0 to 1.0).
     * @param altAddr An optional, alternate address such as a subaddress or LED All Call address (defaults to NULL).
     */
    void PCA9955_setallOutputCurrents(float iref, int altAddr);

    /** Read a range of output current references into an array
     *
     * @param irefs Pointer to an array for up to 16 current references as unsigned chars (0 to 255).
     * @param start The first output to read (defaults to OUTPUT_0).
     * @param end The last output to read (defaults to OUTPUT_15).
     */
    void PCA9955_getOutputCurrents_u8(char* irefs, Output start, Output end);

    /** Set a range of output current references from an array
     *
     * @param irefs Pointer to an array of up to 16 current references as unsigned chars (0 to 255).
     * @param start The first output to set (defaults to OUTPUT_0).
     * @param end The last output to set (defaults to OUTPUT_15).
     * @param altAddr An optional, alternate address such as a subaddress or LED All Call address (defaults to NULL).
     */
    void PCA9955_setOutputCurrents_u8(char* irefs, Output start , Output end , int altAddr);

    /** Set all of the output current references to the same value
     *
     * @param iref The new current reference for all outputs as an unsigned char (0 to 255).
     * @param altAddr An optional, alternate address such as a subaddress or LED All Call address (defaults to NULL).
     */
    void PCA9955_setallOutputCurrents_u8(char iref, int altAddr );

    /** Perform a fault test on all enabled outputs
     *
     * @returns The fault test flags as FaultFlags enum values OR'd together.
     */
    unsigned short PCA9955_faultTest();

    void m_I2C_write(uint16_t addr, char* buffer ,uint16_t len);
    void m_I2C_read(uint16_t addr, char* buffer ,uint16_t len);
    void I2C_Delay_us(uint16_t n);

    //Internal functions
    char PCA9955_read(char reg);
    void PCA9955_write(int addr, char reg, char data);
    void PCA9955_readMulti(char startReg, char* data, int length);
    void PCA9955_writeMulti(int addr, char* data, int length);

    void PCA9955_LED_ANIMATION(int cycle);
    void PCA9955_AmbientLight_reg(void);
    void PCA9955_Set_led(uint8_t id,uint8_t status_led);
#endif /* INC_PCA9955_H_ */
