/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define VID_KTRONIC 0x2C88
#define PID_CDC_KTRONIC 0x030A
#define LED_DRIVER_I2C_ADR1 0x65
#define LED_DRIVER_I2C_ADR2 0x69
#define BTN1_Pin GPIO_PIN_13
#define BTN1_GPIO_Port GPIOC
#define BTN1_EXTI_IRQn EXTI4_15_IRQn
#define COL1_1_Pin GPIO_PIN_0
#define COL1_1_GPIO_Port GPIOC
#define COL2_1_Pin GPIO_PIN_1
#define COL2_1_GPIO_Port GPIOC
#define COL3_1_Pin GPIO_PIN_2
#define COL3_1_GPIO_Port GPIOC
#define COL4_1_Pin GPIO_PIN_3
#define COL4_1_GPIO_Port GPIOC
#define USART_TX_Pin GPIO_PIN_2
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_3
#define USART_RX_GPIO_Port GPIOA
#define LED2_DBG_Pin GPIO_PIN_4
#define LED2_DBG_GPIO_Port GPIOA
#define LED1_DBG_Pin GPIO_PIN_5
#define LED1_DBG_GPIO_Port GPIOA
#define ROW1_2_Pin GPIO_PIN_6
#define ROW1_2_GPIO_Port GPIOA
#define ROW2_2_Pin GPIO_PIN_7
#define ROW2_2_GPIO_Port GPIOA
#define ROW3_2_Pin GPIO_PIN_4
#define ROW3_2_GPIO_Port GPIOC
#define ROW4_2_Pin GPIO_PIN_5
#define ROW4_2_GPIO_Port GPIOC
#define I2C_RESET1_Pin GPIO_PIN_0
#define I2C_RESET1_GPIO_Port GPIOB
#define I2C_RESET2_Pin GPIO_PIN_1
#define I2C_RESET2_GPIO_Port GPIOB
#define I2C_RESET3_Pin GPIO_PIN_2
#define I2C_RESET3_GPIO_Port GPIOB
#define ROW4_3_Pin GPIO_PIN_12
#define ROW4_3_GPIO_Port GPIOB
#define ROW3_3_Pin GPIO_PIN_13
#define ROW3_3_GPIO_Port GPIOB
#define ROW2_3_Pin GPIO_PIN_14
#define ROW2_3_GPIO_Port GPIOB
#define ROW1_3_Pin GPIO_PIN_15
#define ROW1_3_GPIO_Port GPIOB
#define ROW1_1_Pin GPIO_PIN_6
#define ROW1_1_GPIO_Port GPIOC
#define ROW2_1_Pin GPIO_PIN_7
#define ROW2_1_GPIO_Port GPIOC
#define ROW3_1_Pin GPIO_PIN_8
#define ROW3_1_GPIO_Port GPIOC
#define ROW4_1_Pin GPIO_PIN_9
#define ROW4_1_GPIO_Port GPIOC
#define PWM1_BUZ_Pin GPIO_PIN_8
#define PWM1_BUZ_GPIO_Port GPIOA
#define COL4_3_Pin GPIO_PIN_9
#define COL4_3_GPIO_Port GPIOA
#define COL3_3_Pin GPIO_PIN_10
#define COL3_3_GPIO_Port GPIOA
#define SYS_SWDIO_Pin GPIO_PIN_13
#define SYS_SWDIO_GPIO_Port GPIOA
#define SYS_SWCLK_Pin GPIO_PIN_14
#define SYS_SWCLK_GPIO_Port GPIOA
#define PWM2_Pin GPIO_PIN_15
#define PWM2_GPIO_Port GPIOA
#define COL1_2_Pin GPIO_PIN_10
#define COL1_2_GPIO_Port GPIOC
#define COL2_2_Pin GPIO_PIN_11
#define COL2_2_GPIO_Port GPIOC
#define COL3_2_Pin GPIO_PIN_12
#define COL3_2_GPIO_Port GPIOC
#define COL4_2_Pin GPIO_PIN_2
#define COL4_2_GPIO_Port GPIOD
#define COL1_3_Pin GPIO_PIN_3
#define COL1_3_GPIO_Port GPIOB
#define COL2_3_Pin GPIO_PIN_4
#define COL2_3_GPIO_Port GPIOB
#define LEVA_JOYSTICK_SEL_Pin GPIO_PIN_5
#define LEVA_JOYSTICK_SEL_GPIO_Port GPIOB
#define HALL_SENSOR2_Pin GPIO_PIN_8
#define HALL_SENSOR2_GPIO_Port GPIOB
#define HALL_SENSOR2_EXTI_IRQn EXTI4_15_IRQn
#define HALL_SENSOR1_Pin GPIO_PIN_9
#define HALL_SENSOR1_GPIO_Port GPIOB
#define HALL_SENSOR1_EXTI_IRQn EXTI4_15_IRQn
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
