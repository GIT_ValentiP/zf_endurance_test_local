/*
 ******************************************************************************
 *  @file      : display.c
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 14 apr 2021
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : NUOVA_BN_RS485_CAN 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */


/* Includes ----------------------------------------------------------*/
#include "display.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private user code ---------------------------------------------------------*/
void disp_init(){

		//char *buf = malloc(20*sizeof(char));
        LCM1.vp_txt[TOT_TXT]=0x1100;
		LCM1.vp_txt[TASTO1_TXT]=0x1120;
		LCM1.vp_txt[TASTO2_TXT]=0x1140;
		LCM1.vp_txt[TASTO3_TXT]=0x1160;
		LCM1.vp_txt[COUNTS_TXT]=0x1200;
		LCM1.vp_txt[ID_TXT]=0x1220;
		LCM1.vp_txt[STATUS_TXT]=0x1260;



		LCM1.vp_var[COUNT_TOT_VAR]=0x1010;//operation code CMD
		LCM1.vp_var[COUNT_1_VAR]=0x1020;//postazione
		LCM1.vp_var[COUNT_2_VAR]=0x1040;//postazione
		LCM1.vp_var[COUNT_3_VAR]=0x1060;//allarme
		LCM1.vp_var[KEYB_TYPE_VAR]=0x1000;//

		LCM1.vp[0]=0x1000;//ico joy/leva
		//LCM1.vp[1]=0x1160;//ico pulsante piu


		disp_pic(1);
		strcpy(LCM1.txt,"RES ");
		disp_txt(LCM1.vp_txt[STATUS_TXT],LCM1.menu_id,LCM1.txt,4);
		strcpy(LCM1.txt,"NC");
		disp_txt(LCM1.vp_txt[TASTO1_TXT],LCM1.menu_id,LCM1.txt,2);
		strcpy(LCM1.txt,"NC");
		disp_txt(LCM1.vp_txt[TASTO2_TXT],LCM1.menu_id,LCM1.txt,2);
		strcpy(LCM1.txt,"NC");
		disp_txt(LCM1.vp_txt[TASTO3_TXT],LCM1.menu_id,LCM1.txt,2);

		disp_data(LCM1.vp_var[COUNT_TOT_VAR],count_press_ZF[NUM_MAX_KEY_ZF-1].tot,4,0);
		disp_data(LCM1.vp_var[COUNT_1_VAR],0,4,0);
		disp_data(LCM1.vp_var[COUNT_2_VAR],0,4,0);
		disp_data(LCM1.vp_var[COUNT_3_VAR],0,4,0);
		//disp_data(LCM1.vp_var[KEYB_TYPE_VAR],test_type,1,0);
		disp_ico(LCM1.vp_var[KEYB_TYPE_VAR],test_type,test_type);
		LCM1.menu_id=1;
		disp_pic(LCM1.menu_id);

}





void disp_pic(uint16_t id){
uint8_t len,len_data;
uint8_t TX[10];

len_data=0x07;
len=0;
TX[len++]=0x5A;
TX[len++]=0xA5;
TX[len++]=len_data;
TX[len++]=0x82;//LCM_CMD_WR
TX[len++]=0x00;
TX[len++]=0x84;
TX[len++]=0x5A;
TX[len++]=0x01;
TX[len++]=(id>>8);
TX[len++]=(id & 0x00FF);

HAL_UART_Transmit(&huart4, (uint8_t *)&TX, len, 0xFFFF);
}



void disp_txt(uint16_t vp,uint16_t pic_id,char *txt,uint8_t length){
	uint8_t len,len_data,i;
	uint8_t TX[30];
	len_data=length+0x03;
	len=0;
	TX[len++]=0x5A;
	TX[len++]=0xA5;
	TX[len++]=len_data;
	TX[len++]=0x82;//LCM_CMD_WR
	TX[len++]=(vp>>8);
	TX[len++]=(vp & 0x00FF);
	for(i=0;i<length;i++){
		TX[len++]=txt[i];
	}
	HAL_UART_Transmit(&huart4, (uint8_t *)&TX, len, 0xFFFF);

}






void disp_ico(uint16_t vp,uint16_t pic_id,uint16_t val){
	uint8_t len,len_data;
	uint8_t TX[10];
	len_data=0x05;
	len=0;
	TX[len++]=0x5A;
	TX[len++]=0xA5;
	TX[len++]=len_data;
	TX[len++]=0x82;//LCM_CMD_WR
	TX[len++]=(vp>>8);
	TX[len++]=(vp & 0x00FF);
	TX[len++]=0x00;
	TX[len++]=(val & 0x00FF);
	HAL_UART_Transmit(&huart4, (uint8_t *)&TX, len, 0xFFFF);


}





void disp_data(uint16_t vp,uint32_t val,uint8_t len_int,uint8_t len_dec){
	    uint8_t len,len_data;
		uint8_t TX[10];
		count_t value;
		value.tot=val;
		len_data=(0x03+len_int);
		len=0;
		TX[len++]=0x5A;
		TX[len++]=0xA5;
		TX[len++]=len_data;
		TX[len++]=0x82;//LCM_CMD_WR
		TX[len++]=(vp>>8);
		TX[len++]=(vp & 0x00FF);
		TX[len++]=(value.b.HH4);
		TX[len++]=(value.b.HL3);
		TX[len++]=(value.b.LH2);
		TX[len++]=(value.b.LL1);
		HAL_UART_Transmit(&huart4, (uint8_t *)&TX, len, 0xFFFF);


}





void disp_rtc(uint16_t vp,uint16_t year,uint8_t month,uint8_t day,uint8_t hour,uint8_t min){





}




void disp_set_rtc(uint16_t vp,uint16_t year,uint8_t month,uint8_t day,uint8_t hour,uint8_t min){






}

void display_update_txt(void){
uint8_t i;
uint32_t val;

strcpy(LCM1.txt,"NC");
disp_txt(LCM1.vp_txt[TASTO1_TXT],LCM1.menu_id,LCM1.txt,2);
strcpy(LCM1.txt,"NC");
disp_txt(LCM1.vp_txt[TASTO2_TXT],LCM1.menu_id,LCM1.txt,2);
strcpy(LCM1.txt,"NC");
disp_txt(LCM1.vp_txt[TASTO3_TXT],LCM1.menu_id,LCM1.txt,2);

for(i=0;i<NMAX_KEYPRESSED_TEST;i++){
      val=0;
	  if(config_id_key[i] < (NUM_MAX_KEY_ZF-1)){

			sprintf(LCM1.txt,"%.2d",config_id_key[i]);
			val=count_press_ZF[config_id_key[i]].tot;

	  }
	  switch(i){
	 		  	     case(0):
		                     disp_txt(LCM1.vp_txt[TASTO1_TXT],LCM1.menu_id,LCM1.txt,2);
	 		  	             disp_data(LCM1.vp_var[COUNT_1_VAR],val,4,0);
	 		  	    		 break;
	 		  	     case(1):
		                     disp_txt(LCM1.vp_txt[TASTO2_TXT],LCM1.menu_id,LCM1.txt,2);
	 		  	             disp_data(LCM1.vp_var[COUNT_2_VAR],val,4,0);
	 		  	    	     break;
	 		  	     case(2):
		                     disp_txt(LCM1.vp_txt[TASTO3_TXT],LCM1.menu_id,LCM1.txt,2);
	 		  	             disp_data(LCM1.vp_var[COUNT_3_VAR],val,4,0);
	 		  	    	     break;

	 		  	  }

  }
  disp_data(LCM1.vp_var[COUNT_TOT_VAR],count_press_ZF[NUM_MAX_KEY_ZF-1].tot,4,0);

}


void display_update_data(void){
uint8_t i;
uint32_t val;

disp_data(LCM1.vp_var[KEYB_TYPE_VAR],test_type,1,0);

for(i=0;i<NMAX_KEYPRESSED_TEST;i++){

      val=0;
	  if(config_id_key[i] < (NUM_MAX_KEY_ZF-1)){
         //Send_count_answer(config_id_key[i]);
		  val=count_press_ZF[config_id_key[i]].tot;
	  }
	  switch(i){
	 		  	     case(0):
	 		  				 disp_data(LCM1.vp_var[COUNT_1_VAR],val,4,0);
	 		  	    		 break;
	 		  	     case(1):
	 		  	    		 disp_data(LCM1.vp_var[COUNT_2_VAR],val,4,0);
	 		  	    	     break;
	 		  	     case(2):
	 		  	    		 disp_data(LCM1.vp_var[COUNT_3_VAR],val,4,0);
	 		  	    	     break;

	 		  	  }

  }
  disp_data(LCM1.vp_var[COUNT_TOT_VAR],count_press_ZF[NUM_MAX_KEY_ZF-1].tot,4,0);

}
