/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f0xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f0xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "globals.h"
#include "display.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern PCD_HandleTypeDef hpcd_USB_FS;
extern TIM_HandleTypeDef htim6;
extern TIM_HandleTypeDef htim7;
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M0 Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */
  while (1)
  {
  }
  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{
  /* USER CODE BEGIN SVC_IRQn 0 */

  /* USER CODE END SVC_IRQn 0 */
  /* USER CODE BEGIN SVC_IRQn 1 */

  /* USER CODE END SVC_IRQn 1 */
}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{
  /* USER CODE BEGIN PendSV_IRQn 0 */

  /* USER CODE END PendSV_IRQn 0 */
  /* USER CODE BEGIN PendSV_IRQn 1 */

  /* USER CODE END PendSV_IRQn 1 */
}

/**
  * @brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32F0xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f0xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles EXTI line 4 to 15 interrupts.
  */
void EXTI4_15_IRQHandler(void)
{
  /* USER CODE BEGIN EXTI4_15_IRQn 0 */

  /* USER CODE END EXTI4_15_IRQn 0 */
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_8);
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_9);
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_13);
  /* USER CODE BEGIN EXTI4_15_IRQn 1 */

  /* USER CODE END EXTI4_15_IRQn 1 */
}

/**
  * @brief This function handles TIM6 global and DAC channel underrun error interrupts.
  */
void TIM6_DAC_IRQHandler(void)
{
  /* USER CODE BEGIN TIM6_DAC_IRQn 0 */

  /* USER CODE END TIM6_DAC_IRQn 0 */
  HAL_TIM_IRQHandler(&htim6);
  /* USER CODE BEGIN TIM6_DAC_IRQn 1 */

  /* USER CODE END TIM6_DAC_IRQn 1 */
}

/**
  * @brief This function handles TIM7 global interrupt.
  */
void TIM7_IRQHandler(void)
{
  /* USER CODE BEGIN TIM7_IRQn 0 */

  /* USER CODE END TIM7_IRQn 0 */
  HAL_TIM_IRQHandler(&htim7);
  /* USER CODE BEGIN TIM7_IRQn 1 */

  System_Clock_Timer();
  /* USER CODE END TIM7_IRQn 1 */
}

/**
  * @brief This function handles USB global interrupt / USB wake-up interrupt through EXTI line 18.
  */
void USB_IRQHandler(void)
{
  /* USER CODE BEGIN USB_IRQn 0 */

  /* USER CODE END USB_IRQn 0 */
  HAL_PCD_IRQHandler(&hpcd_USB_FS);
  /* USER CODE BEGIN USB_IRQn 1 */

  /* USER CODE END USB_IRQn 1 */
}

/* USER CODE BEGIN 1 */
void System_Clock_Init(void){

    main_clock.ms=0;
    main_clock.ms10=0;
    main_clock.ms100=0;
    main_clock.sec=0;
    main_clock.min=0;
    main_clock.hour=0;
    main_clock.ms10=0;
    main_clock.flag_50ms=0;
    main_clock.flag_100ms=0;
    main_clock.flag_1sec=0;
    main_clock.flag_10sec=0;
    main_clock.flag_1min=0;

}
void System_Clock_Timer(void){

        //if(led.timeout_rx_ms>0){
          //  led.timeout_rx_ms--;
          //}

	    if(scan_row_delay>0){
		    scan_row_delay--;
	    }
        if (++main_clock.ms > 9){//10ms elapsed
            main_clock.ms=0;
//            if(buz1.count_down_10ms >0){
//               buz1.count_down_10ms--;
//            }
//            if(buz1.t_off_10ms >0){
//               buz1.t_off_10ms--;
//            }
            if(led.timeout_blink_10ms>0){
                 led.timeout_blink_10ms--;
            }else{
                led.timeout_blink_10ms=LED_BLINK_PERIOD_10ms;
                led.toggle_led_blink ^=1;
            }
            if ((++main_clock.ms10)==5){//50ms elapsed
               main_clock.flag_50ms=1;
            }
            if (main_clock.ms10 > 9){//100ms elapsed
                main_clock.ms10=0;
                main_clock.flag_50ms=1;
                main_clock.flag_100ms=1;
                if (++main_clock.ms100 > 9){//1sec elapsed
                    main_clock.ms100=0;
                    main_clock.flag_1sec=1;
                    opt3001_rd_time=1;
//                    if (++main_clock.sec > 59){
//                       main_clock.sec=0;
//                       main_clock.flag_1min=1;
//                       if (++main_clock.min >59){
//                          main_clock.min=0;
//                          main_clock.hour++;
//                       }
//                    }
//                    if ((main_clock.sec % 10)==0){
//                        main_clock.flag_10sec=1;
//                    }
                }
            }
        }
}


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
	//GPIO_PinState status;

	switch(GPIO_Pin){
	  case(GPIO_PIN_8)://SENSOR_HALL2 DOWN
			           //status=HAL_GPIO_ReadPin(HALL_SENSOR2_GPIO_Port, HALL_SENSOR2_Pin);
		               if(HAL_GPIO_ReadPin(HALL_SENSOR2_GPIO_Port, HALL_SENSOR2_Pin)==SENSOR_HALL_CLOSE){
		            	   //HAL_GPIO_WritePin(LED1_DBG_GPIO_Port, LED1_DBG_Pin,GPIO_PIN_RESET);
		            	   sensor.b.HALL_DOWN=1;
		            	   sensor.b.DOWN_OLD=0;
		            	   sensor.b.HALL_UP=HAL_GPIO_ReadPin(HALL_SENSOR1_GPIO_Port, HALL_SENSOR1_Pin);
		            	   sensor.b.UP_OLD= sensor.b.HALL_UP;
		               }
			           break;
	  case(GPIO_PIN_9)://SENSOR_HALL1 UP
		               //status=HAL_GPIO_ReadPin(HALL_SENSOR1_GPIO_Port, HALL_SENSOR1_Pin);
					   if(HAL_GPIO_ReadPin(HALL_SENSOR1_GPIO_Port, HALL_SENSOR1_Pin)==SENSOR_HALL_CLOSE){
						   //HAL_GPIO_WritePin(LED1_DBG_GPIO_Port, LED1_DBG_Pin,GPIO_PIN_SET);
						   sensor.b.HALL_UP=1;
						   sensor.b.UP_OLD=0;
						   sensor.b.HALL_DOWN=HAL_GPIO_ReadPin(HALL_SENSOR2_GPIO_Port, HALL_SENSOR2_Pin);
						   sensor.b.DOWN_OLD=sensor.b.HALL_DOWN;
						   if(machine_status.b.STOP0_RUN1==1){
						     //if((sensor.b.UP_OLD==0)&&(sensor.b.HALL_UP==1)){
						  	   if(system_flag.b.b0==0){
						  	      system_flag.b.b0=1;
						  	   }
						  	   count_press_ZF[NUM_MAX_KEY_ZF-1].tot++;
						     //}
						   }
					   }
	  			       break;
	  case(GPIO_PIN_13)://BTN1
		               //status=HAL_GPIO_ReadPin(BTN1_GPIO_Port, BTN1_Pin);
					   if(HAL_GPIO_ReadPin(BTN1_GPIO_Port, BTN1_Pin)==SENSOR_HALL_CLOSE){
						   sensor.b.BTN1=1;
					   }
	                   if(HAL_GPIO_ReadPin(LEVA_JOYSTICK_SEL_GPIO_Port, LEVA_JOYSTICK_SEL_Pin)==SENSOR_HALL_OPEN){
							   sensor.b.JOY_LEVA=1;
							   test_type=ZF_LEVA_TEST;//
					   }else{
							   sensor.b.JOY_LEVA=0;
							   test_type=ZF_JOYSTICK_TEST;//
							}
	                   //disp_data(LCM1.vp_var[KEYB_TYPE_VAR],test_type,1,0);
	                   disp_ico(LCM1.vp_var[KEYB_TYPE_VAR],test_type,test_type);
	                   break;

	  default:
		      break;
	}

	 machine_status.b.b6=sensor.b.HALL_UP;
	 machine_status.b.b7=sensor.b.HALL_DOWN;
}
/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
