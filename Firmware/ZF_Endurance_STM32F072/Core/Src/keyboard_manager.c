
/**
  Generated Main Source File

  Company:
 K-Tronic

  File Name:
    keyboard_mouse_manager.c

  Summary:
  

  Description:
    This header file provides implementations for driver 
*/

#include <stdint.h>
#include <string.h>
#include "main.h"
#include "globals.h"
#include "keyboard_manager.h"
#include "usb_hid_keys.h"
#include "usb_device.h"
#include "gpio.h"
#include "stm32f0xx_it.h"
#include "usbd_cdc_if.h"
#include "PCA9955.h"
#include "usb_cdc_zf_key.h"
#include "display.h"
/*KT-110*/ 

//***********************************************                 COL_0        COL_1        COL_2           COL_3          COL_4         COL_5        COL_6        COL_7         COL_8           COL_9
/*
const uint8_t KEYBOARD1_SCANCODE[NUM_ROW_MAX*NUM_COL_MAX]={      KEY_F10,       KEY_0,       KEY_P,      KEY_OACC,       KEY_DOT,    KEY_RIGHT,     KEY_KP0,      KEY_UACC,  KEY_NUMLOCK,     KEY_PAUSE, //ROW_0
                                                                 KEY_F9,        KEY_9,       KEY_O,         KEY_L,     KEY_COMMA,     KEY_DOWN,   KEY_KPDOT,       KEY_PIU,  KEY_KPSLASH,KEY_SCROLLLOCK, //ROW_1
                                                                 KEY_F8,        KEY_8,       KEY_I,         KEY_K,         KEY_M,     KEY_LEFT, KEY_KPENTER,      KEY_EACC,KEY_KPASTERISK,    KEY_SYSRQ, //ROW_2 
                                                                 KEY_F7,        KEY_7,       KEY_U,         KEY_J,         KEY_N,KEY_RIGHTCTRL,     KEY_KP3,       KEY_KP6,  KEY_KPMINUS,       KEY_F12, //ROW_3                                                                 
                                                                 KEY_F6,        KEY_6,       KEY_Y,         KEY_H,         KEY_B,     KEY_Apps,     KEY_KP2,       KEY_KP5,   KEY_KPPLUS,       KEY_F11, //ROW_4
                                                                 KEY_F5,        KEY_5,       KEY_T,         KEY_G,         KEY_V,KEY_RIGHTMETA,     KEY_KP1,       KEY_KP4,      KEY_KP9,    KEY_PAGEUP, //ROW_5
                                                                 KEY_F4,        KEY_4,       KEY_R,         KEY_F,         KEY_C, KEY_RIGHTALT,    KEY_NONE,      KEY_NONE,      KEY_KP8,      KEY_HOME, //ROW_6
                                                                 KEY_F3,        KEY_3,       KEY_E,         KEY_D,         KEY_X,    KEY_SPACE,      KEY_UP,      KEY_NONE,      KEY_KP7,    KEY_INSERT, //ROW_7                                                             
                                                                 KEY_F2,        KEY_2,       KEY_W,         KEY_S,         KEY_Z,  KEY_LEFTALT,    KEY_NONE,      KEY_NONE, KEY_PAGEDOWN, KEY_BACKSPACE, //ROW_8
                                                                 KEY_F1,        KEY_1,       KEY_Q,         KEY_A, KEY_102_MINOR, KEY_LEFTMETA,KEY_RIGHTSHIFT,   KEY_ENTER,      KEY_END,      KEY_IACC, //ROW_9
                                                                 KEY_ESC,KEY_BACKSLASH_ITA,KEY_TAB,  KEY_CAPSLOCK, KEY_LEFTSHIFT, KEY_LEFTCTRL,KEY_TRATTINI,      KEY_AACC,   KEY_DELETE, KEY_APOSTROFO, //ROW_10
                                                 };
                                                

*/
//***********************************************                                COL_0              COL_1               COL_2              COL_3              COL_4           COL_5              COL_6                 COL_7            COL_8              COL_9            COL_10                COL_11
//***********************************************                                COL1_1             COL2_1              COL3_1             COL4_1             COL1_2          COL2_2             COL3_2                COL4_2           COL1_3             COL2_3           COL3_3                COL4_3
const uint8_t KEYBOARD_ZF_LEVA_SCANCODE[NUM_ROW_ZF_MAX*NUM_COL_ZF_MAX]={   KEY_TRIMPORT_M_1, KEY_TRIMPORT_P_1, KEY_TRIMSTBD_M_1,  KEY_TRIMSTBD_P_1, KEY_TRIMPORT_M_2,  KEY_TRIMPORT_P_2,   KEY_TRIMSTBD_M_2,  KEY_TRIMSTBD_P_2,KEY_TRIMPORT_M_3,  KEY_TRIMPORT_P_3,  KEY_TRIMSTBD_M_3,  KEY_TRIMSTBD_P_3, //ROW_0    ROW1_1
																			   KEY_INCDEC_1,        KEY_PIU_1,     KEY_CTRL23_1,          KEY_NONE,     KEY_INCDEC_2,         KEY_PIU_2,       KEY_CTRL23_2,          KEY_NONE,    KEY_INCDEC_3,         KEY_PIU_3,      KEY_CTRL23_3,          KEY_NONE, //ROW_1    ROW2_1
																			   KEY_WARMUP_1,      KEY_TROLL_1,       KEY_MENO_1,          KEY_NONE,     KEY_WARMUP_2,       KEY_TROLL_2,         KEY_MENO_2,          KEY_NONE,    KEY_WARMUP_3,       KEY_TROLL_3,        KEY_MENO_3,          KEY_NONE, //ROW_2    ROW3_1
																				KEY_CTRL1_1,   KEY_ONELEVER_1,         KEY_NONE,          KEY_NONE,      KEY_CTRL1_2,    KEY_ONELEVER_2,           KEY_NONE,          KEY_NONE,     KEY_CTRL1_3,    KEY_ONELEVER_3,          KEY_NONE,          KEY_NONE, //ROW_3    ROW4_1
																		   KEY_TRIMPORT_M_2, KEY_TRIMPORT_P_2, KEY_TRIMSTBD_M_2,  KEY_TRIMSTBD_P_2, KEY_TRIMPORT_M_2,  KEY_TRIMPORT_P_2,   KEY_TRIMSTBD_M_2,  KEY_TRIMSTBD_P_2,        KEY_NONE,          KEY_NONE,          KEY_NONE,          KEY_NONE, //ROW_4    ROW1_2
																			   KEY_INCDEC_2,        KEY_PIU_2,     KEY_CTRL23_2,          KEY_NONE,     KEY_INCDEC_2,         KEY_PIU_2,       KEY_CTRL23_2,          KEY_NONE,        KEY_NONE,          KEY_NONE,          KEY_NONE,          KEY_NONE, //ROW_5    ROW2_2
																			   KEY_WARMUP_2,      KEY_TROLL_2,       KEY_MENO_2,          KEY_NONE,     KEY_WARMUP_2,       KEY_TROLL_2,         KEY_MENO_2,          KEY_NONE,        KEY_NONE,          KEY_NONE,          KEY_NONE,          KEY_NONE, //ROW_6    ROW3_2
																				KEY_CTRL1_2,   KEY_ONELEVER_2,         KEY_NONE,          KEY_NONE,      KEY_CTRL1_2,    KEY_ONELEVER_2,           KEY_NONE,          KEY_NONE,        KEY_NONE,          KEY_NONE,          KEY_NONE,          KEY_NONE, //ROW_7    ROW4_2
																		   KEY_TRIMPORT_M_3, KEY_TRIMPORT_P_3, KEY_TRIMSTBD_M_3,  KEY_TRIMSTBD_P_3,         KEY_NONE,          KEY_NONE,           KEY_NONE,          KEY_NONE,KEY_TRIMPORT_M_3,  KEY_TRIMPORT_P_3,  KEY_TRIMSTBD_M_3,  KEY_TRIMSTBD_P_3, //ROW_8    ROW1_3
																			   KEY_INCDEC_3,        KEY_PIU_3,     KEY_CTRL23_3,          KEY_NONE,         KEY_NONE,          KEY_NONE,           KEY_NONE,          KEY_NONE,    KEY_INCDEC_3,         KEY_PIU_3,      KEY_CTRL23_3,          KEY_NONE, //ROW_9    ROW2_3
																			   KEY_WARMUP_3,      KEY_TROLL_3,       KEY_MENO_3,          KEY_NONE,         KEY_NONE,          KEY_NONE,           KEY_NONE,          KEY_NONE,    KEY_WARMUP_3,       KEY_TROLL_3,        KEY_MENO_3,          KEY_NONE, //ROW_10   ROW3_3
																				KEY_CTRL1_3,   KEY_ONELEVER_3,         KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,           KEY_NONE,          KEY_NONE,     KEY_CTRL1_3,    KEY_ONELEVER_3,          KEY_NONE,          KEY_NONE, //ROW_11   ROW4_3

};
const uint8_t KEYBOARD_ZF_JOY_SCANCODE[NUM_ROW_ZF_MAX*NUM_COL_ZF_MAX]={     KEY_JOY_BOOST_1, KEY_JOY_CTRL123_1,         KEY_NONE,          KEY_NONE,  KEY_JOY_BOOST_2, KEY_JOY_CTRL123_2,           KEY_NONE,          KEY_NONE,  KEY_JOY_BOOST_3, KEY_JOY_CTRL123_3,          KEY_NONE,          KEY_NONE, //ROW_0  ROW1_1
		                                                                  KEY_JOY_IANCHOR_1,KEY_JOY_THRUSTER_1,         KEY_NONE,          KEY_NONE,KEY_JOY_IANCHOR_2,KEY_JOY_THRUSTER_2,           KEY_NONE,          KEY_NONE,KEY_JOY_IANCHOR_3,KEY_JOY_THRUSTER_3,          KEY_NONE,          KEY_NONE, //ROW_1  ROW2_1
																		           KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,           KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,          KEY_NONE,          KEY_NONE, //ROW_2  ROW3_1
																				   KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,           KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,          KEY_NONE,          KEY_NONE, //ROW_3
																	        KEY_JOY_BOOST_2, KEY_JOY_CTRL123_2,         KEY_NONE,          KEY_NONE,  KEY_JOY_BOOST_2, KEY_JOY_CTRL123_2,           KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,          KEY_NONE,          KEY_NONE, //ROW_4
                                                                          KEY_JOY_IANCHOR_2,KEY_JOY_THRUSTER_2,         KEY_NONE,          KEY_NONE,KEY_JOY_IANCHOR_2,KEY_JOY_THRUSTER_2,           KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,          KEY_NONE,          KEY_NONE, //ROW_5
																			       KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,           KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,          KEY_NONE,          KEY_NONE, //ROW_6
																				   KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,           KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,          KEY_NONE,          KEY_NONE, //ROW_7
																		    KEY_JOY_BOOST_3, KEY_JOY_CTRL123_3,         KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,           KEY_NONE,          KEY_NONE,  KEY_JOY_BOOST_3, KEY_JOY_CTRL123_3,          KEY_NONE,          KEY_NONE, //ROW_8
							                                              KEY_JOY_IANCHOR_3,KEY_JOY_THRUSTER_3,         KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,           KEY_NONE,          KEY_NONE,KEY_JOY_IANCHOR_3,KEY_JOY_THRUSTER_3,          KEY_NONE,          KEY_NONE, //ROW_9
																			       KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,           KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,          KEY_NONE,          KEY_NONE, //ROW_10
										 	          		   				       KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,           KEY_NONE,          KEY_NONE,         KEY_NONE,          KEY_NONE,          KEY_NONE,          KEY_NONE, //ROW_11
                                                 };

// *****************************************************************************
// *****************************************************************************
// Section: File Scope or Global Constants
// *****************************************************************************
// *****************************************************************************


// *****************************************************************************
// *****************************************************************************
// Section: File Scope Data Types
// *****************************************************************************
// *****************************************************************************

/** VARIABLES ******************************************************/
/* Some processors have a limited range of RAM addresses where the USB module
 * is able to access.  The following section is for those devices.  This section
 * assigns the buffers that need to be used by the USB module into those
 * specific areas.
 */
void Send_data_UART(void);
void Check_Btn_Pressed(uint8_t scan_code);
void Check_Btn_Pressed_ZF(uint8_t scan_code);
/******************************************************************************
 * Function:        SetRowsOutput(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        
 *                  
 *
 * Note:            None
 *****************************************************************************/
void SetRowsOutput(uint8_t nrow)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};


    /*Configure GPIO pins : PAPin PAPin PAPin PAPin */
     GPIO_InitStruct.Pin = ROW1_2_Pin|ROW2_2_Pin;
     GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
     GPIO_InitStruct.Pull = GPIO_PULLUP;
     HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

     /*Configure GPIO pins : PCPin PCPin PCPin PCPin
                              PCPin PCPin */
     GPIO_InitStruct.Pin = ROW3_2_Pin|ROW4_2_Pin|ROW1_1_Pin|ROW2_1_Pin|ROW3_1_Pin|ROW4_1_Pin;
     GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
     GPIO_InitStruct.Pull = GPIO_PULLUP;
     HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

     /*Configure GPIO pins : PBPin PBPin PBPin PBPin
                              PBPin PBPin PBPin */
     GPIO_InitStruct.Pin = ROW4_3_Pin|ROW3_3_Pin|ROW2_3_Pin|ROW1_3_Pin;
     GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
     GPIO_InitStruct.Pull = GPIO_PULLUP;
     HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);


//   GPIO_PinState     HAL_GPIO_ReadPin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
//   void              HAL_GPIO_WritePin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, GPIO_PinState PinState);
    switch(nrow){
        case(0):      
		        HAL_GPIO_WritePin(ROW1_1_GPIO_Port,ROW1_1_Pin, GPIO_PIN_RESET);
				GPIO_InitStruct.Pin =ROW1_1_Pin ;
				GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
				GPIO_InitStruct.Pull = GPIO_PULLUP;
				GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
                HAL_GPIO_Init(ROW1_1_GPIO_Port, &GPIO_InitStruct);

                break;
        case(1):
		        HAL_GPIO_WritePin(ROW2_1_GPIO_Port,ROW2_1_Pin, GPIO_PIN_RESET);
				GPIO_InitStruct.Pin =ROW2_1_Pin ;
				GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
				GPIO_InitStruct.Pull = GPIO_PULLUP;
				HAL_GPIO_Init(ROW2_1_GPIO_Port, &GPIO_InitStruct);
                break;
        case(2):
				HAL_GPIO_WritePin(ROW3_1_GPIO_Port,ROW3_1_Pin, GPIO_PIN_RESET);
				GPIO_InitStruct.Pin =ROW3_1_Pin ;
				GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
				GPIO_InitStruct.Pull = GPIO_PULLUP;
				HAL_GPIO_Init(ROW3_1_GPIO_Port, &GPIO_InitStruct);
                break;        
        case(3):
				HAL_GPIO_WritePin(ROW4_1_GPIO_Port,ROW4_1_Pin, GPIO_PIN_RESET);
				GPIO_InitStruct.Pin =ROW4_1_Pin ;
				GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
				GPIO_InitStruct.Pull = GPIO_PULLUP;
				HAL_GPIO_Init(ROW4_1_GPIO_Port, &GPIO_InitStruct);
                break;
        case(4):
				HAL_GPIO_WritePin(ROW1_2_GPIO_Port,ROW1_2_Pin, GPIO_PIN_RESET);
				GPIO_InitStruct.Pin =ROW1_2_Pin ;
				GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
				GPIO_InitStruct.Pull = GPIO_PULLUP;
				HAL_GPIO_Init(ROW1_2_GPIO_Port, &GPIO_InitStruct);
                break;
        case(5):              
				HAL_GPIO_WritePin(ROW2_2_GPIO_Port,ROW2_2_Pin, GPIO_PIN_RESET);
				GPIO_InitStruct.Pin =ROW2_2_Pin ;
				GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
				GPIO_InitStruct.Pull = GPIO_PULLUP;
				HAL_GPIO_Init(ROW2_2_GPIO_Port, &GPIO_InitStruct);
                break;
        case(6):
				HAL_GPIO_WritePin(ROW3_2_GPIO_Port,ROW3_2_Pin, GPIO_PIN_RESET);
				GPIO_InitStruct.Pin =ROW3_2_Pin ;
				GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
				GPIO_InitStruct.Pull = GPIO_PULLUP;
				HAL_GPIO_Init(ROW3_2_GPIO_Port, &GPIO_InitStruct);
                break;
        case(7):        
				HAL_GPIO_WritePin(ROW4_2_GPIO_Port,ROW4_2_Pin, GPIO_PIN_RESET);
				GPIO_InitStruct.Pin =ROW4_2_Pin ;
				GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
				GPIO_InitStruct.Pull = GPIO_PULLUP;
				HAL_GPIO_Init(ROW4_2_GPIO_Port, &GPIO_InitStruct);
                break;        
        case(8):
				HAL_GPIO_WritePin(ROW1_3_GPIO_Port,ROW1_3_Pin, GPIO_PIN_RESET);
				GPIO_InitStruct.Pin =ROW1_3_Pin ;
				GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
				GPIO_InitStruct.Pull = GPIO_PULLUP;
				HAL_GPIO_Init(ROW1_3_GPIO_Port, &GPIO_InitStruct);
                break;
        case(9): 
				HAL_GPIO_WritePin(ROW2_3_GPIO_Port,ROW2_3_Pin, GPIO_PIN_RESET);
				GPIO_InitStruct.Pin =ROW2_3_Pin ;
				GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
				GPIO_InitStruct.Pull = GPIO_PULLUP;
				HAL_GPIO_Init(ROW2_3_GPIO_Port, &GPIO_InitStruct);
                break;        
        case(10): 
				HAL_GPIO_WritePin(ROW3_3_GPIO_Port,ROW3_3_Pin, GPIO_PIN_RESET);
				GPIO_InitStruct.Pin =ROW3_3_Pin ;
				GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
				GPIO_InitStruct.Pull = GPIO_PULLUP;
				HAL_GPIO_Init(ROW3_3_GPIO_Port, &GPIO_InitStruct);
                break;            
        case(11):
				HAL_GPIO_WritePin(ROW4_3_GPIO_Port,ROW4_3_Pin, GPIO_PIN_RESET);
				GPIO_InitStruct.Pin =ROW4_3_Pin ;
				GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
				GPIO_InitStruct.Pull = GPIO_PULLUP;
				HAL_GPIO_Init(ROW4_3_GPIO_Port, &GPIO_InitStruct);
				break;
        default:
                break;
    }
    
   
}




/******************************************************************************
 * Function:        SetColsInput(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        
 *                  
 *
 * Note:            None
 *****************************************************************************/
void SetColsInput(uint8_t ncol)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	GPIO_InitStruct.Pin = COL1_1_Pin|COL2_1_Pin|COL3_1_Pin|COL4_1_Pin|COL1_2_Pin|COL2_2_Pin|COL3_2_Pin;
	if(ncol!=0xFF){
	  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	  GPIO_InitStruct.Pull = GPIO_PULLUP;
	}else{
		 GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	     GPIO_InitStruct.Pull = GPIO_PULLUP;
	     GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	}
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pin : PtPin */
	GPIO_InitStruct.Pin = COL4_2_Pin;
	if(ncol!=0xFF){
		  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
		  GPIO_InitStruct.Pull = GPIO_PULLUP;
    }else{
			 GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		     GPIO_InitStruct.Pull = GPIO_PULLUP;
		     GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		}
	HAL_GPIO_Init(COL4_2_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : PBPin PBPin PBPin */
	GPIO_InitStruct.Pin = COL1_3_Pin|COL2_3_Pin;
	if(ncol!=0xFF){
		  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
		  GPIO_InitStruct.Pull = GPIO_PULLUP;
	}else{
			 GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		     GPIO_InitStruct.Pull = GPIO_PULLUP;
		     GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		 }
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pins : PAPin PAPin */
	  GPIO_InitStruct.Pin = COL4_3_Pin|COL3_3_Pin;
	  if(ncol!=0xFF){
	  		  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	  		  GPIO_InitStruct.Pull = GPIO_PULLUP;
	  	}else{
	  			 GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  		     GPIO_InitStruct.Pull = GPIO_PULLUP;
	  		     GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	  		 }
	  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}



/******************************************************************************
 * Function:       ReadColStatus(uint8_t ncol)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        
 *                  
 *
 * Note:            None
 *****************************************************************************/
uint8_t  ReadColStatus(uint8_t ncol)
{
   uint8_t ret_val;
   ret_val=1;
   switch(ncol){
       case(0):
               ret_val=(uint8_t) HAL_GPIO_ReadPin(COL1_1_GPIO_Port,COL1_1_Pin);
               break;
       case(1):
			   ret_val=(uint8_t) HAL_GPIO_ReadPin(COL2_1_GPIO_Port,COL2_1_Pin);
               break;
       case(2):
			   ret_val=(uint8_t) HAL_GPIO_ReadPin(COL3_1_GPIO_Port,COL3_1_Pin);
               break;
       case(3):
			   ret_val=(uint8_t) HAL_GPIO_ReadPin(COL4_1_GPIO_Port,COL4_1_Pin);
               break;
       case(4):
		       ret_val=(uint8_t) HAL_GPIO_ReadPin(COL1_2_GPIO_Port,COL1_2_Pin);
               break;
       case(5):
		       ret_val=(uint8_t) HAL_GPIO_ReadPin(COL2_2_GPIO_Port,COL2_2_Pin);
               break;
       case(6):
		       ret_val=(uint8_t) HAL_GPIO_ReadPin(COL3_2_GPIO_Port,COL3_2_Pin);
               break;
       case(7):
		       ret_val=(uint8_t) HAL_GPIO_ReadPin(COL4_2_GPIO_Port,COL4_2_Pin);
               break;
       case(8):
		       ret_val=(uint8_t) HAL_GPIO_ReadPin(COL1_3_GPIO_Port,COL1_3_Pin);
               break;
       case(9):
		       ret_val=(uint8_t) HAL_GPIO_ReadPin(COL2_3_GPIO_Port,COL2_3_Pin);
               break;
       case(10):
      		   ret_val=(uint8_t) HAL_GPIO_ReadPin(COL3_3_GPIO_Port,COL3_3_Pin);
               break;
       case(11):
      		   ret_val=(uint8_t) HAL_GPIO_ReadPin(COL4_3_GPIO_Port,COL4_3_Pin);
               break;
       default:
               ret_val=1;
               break;
   }
   return(ret_val);
}

void SetLeds(void){
    uint8_t id;  
  for(id=0;id<NMAX_LED;id++){
    
      switch(id){
          case(0):
                  switch(led.status_led[id]){
                      case(LED_STATUS_ON):
		                                    HAL_GPIO_WritePin(LED2_DBG_GPIO_Port,LED2_DBG_Pin, GPIO_PIN_RESET);//OUT00_SetLow();
                                            break;
                      case(LED_STATUS_BLINK):
                                            if(led.toggle_led_blink){
                                            	 HAL_GPIO_WritePin(LED2_DBG_GPIO_Port,LED2_DBG_Pin, GPIO_PIN_RESET);//OUT00_SetLow();
                                            }else{
                                            	 HAL_GPIO_WritePin(LED2_DBG_GPIO_Port,LED2_DBG_Pin, GPIO_PIN_SET);//OUT00_SetHigh();
                                            } 
                                            break;
                      default:
                    	       HAL_GPIO_WritePin(LED2_DBG_GPIO_Port,LED2_DBG_Pin, GPIO_PIN_SET);//OUT00_SetHigh();
                               break;
                  }

                  break;
        case(1):
                switch(led.status_led[id]){
                      case(LED_STATUS_ON):
		                                    HAL_GPIO_WritePin(PWM1_BUZ_GPIO_Port,PWM1_BUZ_Pin, GPIO_PIN_RESET);//OUT01_SetLow();
                                            break;
                      case(LED_STATUS_BLINK):
                                            if(led.toggle_led_blink){
                                            	HAL_GPIO_WritePin(PWM1_BUZ_GPIO_Port,PWM1_BUZ_Pin, GPIO_PIN_RESET);// OUT01_SetLow();
                                            }else{
                                            	HAL_GPIO_WritePin(PWM1_BUZ_GPIO_Port,PWM1_BUZ_Pin, GPIO_PIN_SET);//OUT01_SetHigh();
                                            } 
                                            break;
                      default:
                    	       HAL_GPIO_WritePin(PWM1_BUZ_GPIO_Port,PWM1_BUZ_Pin, GPIO_PIN_SET);//OUT01_SetHigh();
                               break;
                }

                break;  
         case(2):
                  switch(led.status_led[id]){
                      case(LED_STATUS_ON):
		                                    HAL_GPIO_WritePin(PWM2_GPIO_Port,PWM2_Pin, GPIO_PIN_RESET);//OUT02_SetLow();
                                            break;
                      case(LED_STATUS_BLINK):
                                            if(led.toggle_led_blink){
                                            	HAL_GPIO_WritePin(PWM2_GPIO_Port,PWM2_Pin, GPIO_PIN_RESET); // OUT02_SetLow();
                                            }else{
                                            	HAL_GPIO_WritePin(PWM2_GPIO_Port,PWM2_Pin, GPIO_PIN_SET);//OUT02_SetHigh();
                                            } 
                                            break;
                      default:
                    	       HAL_GPIO_WritePin(PWM2_GPIO_Port,PWM2_Pin, GPIO_PIN_SET);//OUT02_SetHigh();
                               break;
                  }

                  break;
        case(3):
                switch(led.status_led[id]){
                      case(LED_STATUS_ON):
		                                    HAL_GPIO_WritePin(LED1_DBG_GPIO_Port, LED1_DBG_Pin,GPIO_PIN_SET);////OUT03_SetLow();
                                            break;
                      case(LED_STATUS_BLINK):
                                            if(led.toggle_led_blink){
                                            	HAL_GPIO_WritePin(LED1_DBG_GPIO_Port, LED1_DBG_Pin,GPIO_PIN_SET);// OUT03_SetLow();
                                            }else{
                                            	HAL_GPIO_WritePin(LED1_DBG_GPIO_Port, LED1_DBG_Pin,GPIO_PIN_RESET);//OUT03_SetHigh();
                                            } 
                                            break;
                      default:
                    	       HAL_GPIO_WritePin(LED1_DBG_GPIO_Port, LED1_DBG_Pin,GPIO_PIN_RESET);//OUT03_SetHigh();
                               break;
                }

                break;  
         case(4):
                  switch(led.status_led[id]){
                      case(LED_STATUS_ON):
                                            //OUT04_SetLow();
                                            break;
                      case(LED_STATUS_BLINK):
                                            if(led.toggle_led_blink){
                                                 //OUT04_SetLow();
                                            }else{
                                               //OUT04_SetHigh();
                                            } 
                                            break;
                      default:
                               //OUT04_SetHigh();
                               break;
                  }

                  break;
        case(5):
                switch(led.status_led[id]){
                      case(LED_STATUS_ON):
                                            //OUT05_SetLow();
                                            break;
                      case(LED_STATUS_BLINK):
                                            if(led.toggle_led_blink){
                                                 //OUT05_SetLow();
                                            }else{
                                                 //OUT05_SetHigh();
                                            } 
                                            break;
                      default:
                               //OUT05_SetHigh();
                               break;
                }

                break;                 
                
                
                
      }

    if(led.status_led[id]==LED_STATUS_BLINK){
		 if(led.toggle_led_blink){
			 PCA9955_Set_led(id,LED_STATUS_ON);
		 }else{
			 PCA9955_Set_led(id,LED_STATUS_OFF);
		 }
    }
  }  
    


    
}
void RxLed_Init(void){
    uint8_t id;
    led.pos_byte_rx=0;
    led.buffer_rx[0]=0;
    led.buffer_rx[1]=0;
    led.buffer_rx[2]=0;
    led.timeout_rx_ms=0;
    led.rx_status=0;
    for(id=0;id<NUM_MAX_KEY_ZF;id++){
    	if(id<NMAX_LED){
           led.status_led[id]=LED_STATUS_OFF;
    	}
    	if(id<NMAX_KEYPRESSED_TEST){
    		config_id_key[id]=0xFF;
    	}
    	count_press_ZF[id].tot=0;
    }


}
void RxLed_Process(void){
//uint8_t data;

}

void Check_Btn_Pressed(uint8_t scan_code){

}

void Check_Btn_Pressed_ZF(uint8_t scan_code){


 uint8_t found=0;


 if(test_type==ZF_LEVA_TEST){//LEVA
    if(scan_code<13){
    	count_press_ZF[scan_code-1].tot++;
    	found=1;
    }else if(scan_code<29){
    	     count_press_ZF[scan_code-5].tot++;
			 found=1;
           }else if(scan_code<45){
        	        count_press_ZF[scan_code-9].tot++;
				    found=1;
                 }
 }else{//JOYSTICK
	    if(scan_code<5){
		  count_press_ZF[scan_code-1].tot++;
	      found=1;
	    }else if(scan_code<21){
	    	     count_press_ZF[scan_code-5].tot++;
				 found=1;
	          }else if(scan_code<37){
	        	        count_press_ZF[scan_code-9].tot++;
					    found=1;
	                }
     }

//If Event link to button pressed
   if(found==1){
//	   if(system_flag.b.b0==0){
//	      system_flag.b.b0=1;
//	   }
//
//	   count_press_ZF[NUM_MAX_KEY_ZF-1].tot++;
   }

}

void Send_data_UART(void){
 
 keys_word_t chksum;
 
 chksum.bitmap=0;
 Tx_Message[0] =HEIDENHAIN_START_KEY_FRAME;//getch();
 chksum.bitmap+=Tx_Message[0];

 Tx_Message[1]=keys_tx[2].byte.HB;
 chksum.bitmap+=Tx_Message[1];
        
 Tx_Message[2]=keys_tx[2].byte.LB;
 chksum.bitmap+=Tx_Message[2];
 
 Tx_Message[3]=keys_tx[1].byte.HB;
 chksum.bitmap+=Tx_Message[3];
        
 Tx_Message[4]=keys_tx[1].byte.LB;
 chksum.bitmap+=Tx_Message[4];
 
 Tx_Message[5]=keys_tx[0].byte.HB;
 chksum.bitmap+=Tx_Message[5];
        
 Tx_Message[6]=keys_tx[0].byte.LB;
 chksum.bitmap+=Tx_Message[6];
         
 Tx_Message[7] = chksum.byte.HB;//getch();
       
 Tx_Message[8] =chksum.byte.LB;//getch();

 /* Make sure that the CDC driver is ready for a transmission.
  * Send message on USB CDC
 */

 CDC_Transmit_FS(Tx_Message, 9);//putUSBUSART(Tx_Message,9);

        
   
 
}
 void Zero_counter_set(uint8_t id){
 
 uint8_t id_key;

 id_key=id;
 if(id > (NUM_MAX_KEY_ZF-2)){
  for(id_key=0;id_key< NUM_MAX_KEY_ZF;id_key++){
     
	  count_press_ZF[id_key].tot=0;
      
  }  

 }else{
	 count_press_ZF[id_key].tot=0;
 }
  
      
}

 void Receive_request_uart(uint8_t* readBuffer, uint32_t *Len){
uint8_t i;
uint32_t numBytesRead;
uint32_t val;
uint8_t frame_ok=1;
numBytesRead = *Len;
uint8_t id_led;
	        /* For every byte that was read... */

   switch(numBytesRead) {
     case(3):
               i=0;
	           while((i<numBytesRead) && (frame_ok==1))
	            {
	                switch(i){
	                        case(0):
	                                //while(EUSART1_is_tx_done());
	                                switch(readBuffer[0]){
	                                      case(LED_STATUS_ON):
	                                      case(LED_STATUS_BLINK):
	                                      case(LED_STATUS_OFF):
	                                      case(READ_COUNTER_STATUS):
	                                      case(ZERO_COUNTER_SET):
	                                      case(START_STOP_CMD):
	                                      case(SET_FRAME_RATE):
	                                                                 frame_ok=1;
	                                                                 break;

	                                      default:
	                                               frame_ok=0;
	                                                break;
	                                }
	                        break;
	                        case(1):
									 switch(readBuffer[0]){
											  case(LED_STATUS_ON):
											  case(LED_STATUS_BLINK):
											  case(LED_STATUS_OFF):
											  case(READ_COUNTER_STATUS):
											  case(ZERO_COUNTER_SET):

																		if((readBuffer[i]>0x2F)&&(readBuffer[i]<0x3A)){
																		   frame_ok=1;
																		}else{
																		   frame_ok=0;
																		}
																		break;
											  case(START_STOP_CMD):
											  case(SET_FRAME_RATE):
											  		                    val=0;
                                                                        val=readBuffer[i];
											 	                        frame_ok=1;
				                                                        break;
											  default:
													   frame_ok=0;
														break;
									 }

	                                 break;
	                          case(2):


	                                       switch(readBuffer[0]){
													  case(LED_STATUS_ON):
													  case(LED_STATUS_BLINK):
													  case(LED_STATUS_OFF):
													                       if((readBuffer[2]>0x2F)&&(readBuffer[2]<0x3A)){

																			   id_led=(readBuffer[1]-0x30)*10;
																			   id_led+=(readBuffer[2]-0x30);
																			   if(id_led < NMAX_LED){
																				  led.status_led[id_led] =readBuffer[0];
																				  PCA9955_Set_led(id_led,led.status_led[id_led]);
																				  Send_ACK_answer(readBuffer[0]);
																			   }
													                       }
													                       break;
													  case(READ_COUNTER_STATUS):
		                                                                        if((readBuffer[2]>0x2F)&&(readBuffer[2]<0x3A)){
																					   id_led=(readBuffer[1]-0x30)*10;
																					   id_led+=(readBuffer[2]-0x30);
		                                                                               Send_count_answer(id_led);
		                                                                               if(machine_status.b.POR==1){
																						   machine_status.b.POR=0;
																						   strcpy(LCM1.txt,"STOP");
																						   disp_txt(LCM1.vp_txt[STATUS_TXT],LCM1.menu_id,LCM1.txt,4);
		                                                                               }
		                                                                        }
															                    break;
													  case(ZERO_COUNTER_SET):
		                                                                     if((readBuffer[2]>0x2F)&&(readBuffer[2]<0x3A)){
																			     id_led=(readBuffer[1]-0x30)*10;
																			     id_led+=(readBuffer[2]-0x30);
																			     Zero_counter_set(id_led);
			                                                                     Send_count_answer(id_led);
			                                                                     display_update_data();
		                                                                     }
															                 break;
													  case(SET_FRAME_RATE):
																			val*=256;
																			val+=readBuffer[i];
																			frame_rate_tx=(uint16_t)val;
																			Send_ACK_answer(readBuffer[0]);
																			break;
													  case(START_STOP_CMD):
		                                                                    val*=256;
																			val+=readBuffer[i];
																			if(val==0){
																				 machine_status.b.STOP0_RUN1=0;
																				 strcpy(LCM1.txt,"STOP");

																			}else{
																				 machine_status.b.STOP0_RUN1=1;
																				 strcpy(LCM1.txt,"RUN ");

																			}
																			disp_txt(LCM1.vp_txt[STATUS_TXT],LCM1.menu_id,LCM1.txt,4);
																			Send_ACK_answer(readBuffer[0]);
																			break;
													  default:
														      break;
	                                       }


	                                     frame_ok=0;
	                              break;

	                  }
	                i++;
	            }
                break;
     case(4):{
	                i=0;

	                while((i<numBytesRead) && (frame_ok==1)){
	                	         switch(i){
											case(0):
													//while(EUSART1_is_tx_done());
													switch(readBuffer[0]){
														  case(CONFIG_KEY_TEST):
																 frame_ok=1;
																 break;
														  default:
																 frame_ok=0;
																 break;
													}
											break;
											case(1):
													if(readBuffer[1]<(NUM_MAX_KEY_ZF-1)){
													   config_id_key[i-1]=readBuffer[i];

													}else{
														config_id_key[i-1]=0xFF;
													}
											        frame_ok=1;
													break;
											case(2):
													if(readBuffer[2]<(NUM_MAX_KEY_ZF-1)){
														config_id_key[i-1]=readBuffer[i];

													}else{
														config_id_key[i-1]=0xFF;
													}
											        frame_ok=1;
													break;
											case(3):
													if(readBuffer[3]<(NUM_MAX_KEY_ZF-1)){
													   config_id_key[i-1]=readBuffer[i];

													}else{
														config_id_key[i-1]=0xFF;
													}

											        Tx_Counter_ZF();
											        display_update_txt();
											        if(machine_status.b.POR==1){
											          machine_status.b.POR=0;
											          strcpy(LCM1.txt,"STOP");
											          disp_txt(LCM1.vp_txt[STATUS_TXT],LCM1.menu_id,LCM1.txt,4);
											        }
													frame_ok=0;
													break;

										  }
	                		              i++;
	                }//end_while
	            }
                break;
     case(6):{//set counter value
    	                i=0;

    	                while((i<numBytesRead) && (frame_ok==1)){
    	                	         switch(i){
    											case(0):
    													//while(EUSART1_is_tx_done());
    													switch(readBuffer[0]){
    														  case(SET_COUNTER_VALUE):
    																 frame_ok=1;
    																 break;
    														  default:
    																 frame_ok=0;
    																 break;
    													}
    											        break;
    											case(1):
    													if(readBuffer[1]<NUM_MAX_KEY_ZF){
    														id_led=readBuffer[i];
    													   frame_ok=1;
    													   val=0;
    													}else{
    													   frame_ok=0;
    													}
    													break;
    											case(2):
    													val=0;
		                                                val+=readBuffer[i];
    													frame_ok=1;

    													break;
    											case(3):
														val*=256;
														val+=readBuffer[i];
														frame_ok=1;
														break;

    											case(4):
														val*=256;
														val+=readBuffer[i];
														frame_ok=1;

														break;
    											case(5):
														val*=256;
														val+=readBuffer[i];

														 if(id_led < NUM_MAX_KEY_ZF){
															 count_press_ZF[id_led].tot=val;
															 Send_count_answer(id_led);
															 display_update_data();
														 }
														 frame_ok=0;


														break;

    										  }
    	                		              i++;
    	                }//end_while
    	            }
                    break;
     default:
    	     break;
   }


 }
 
 void Send_count_answer(uint8_t id){
 
 keys_word_t chksum;
 uint8_t id_key;
 chksum.bitmap=0;
 Tx_Message[0] =START_BYTE_SINGLE_COUNTER;//
 chksum.bitmap+=Tx_Message[0];

 Tx_Message[1]=id;//count_press[id].b.HH4;
 chksum.bitmap+=Tx_Message[1];
 id_key=id;
 if(id > (NUM_MAX_KEY_ZF-2)){
  id_key=(NUM_MAX_KEY_ZF-1);
  Tx_Message[2]=machine_status.bitmap;//count_press[id].b.HH4;;//count_press[id].b.HH4;
 }else{
  Tx_Message[2]=0x00;//count_press[id].b.HH4;;//count_press[id].b.HH4;
 }
 
 chksum.bitmap+=Tx_Message[2];
 
 Tx_Message[3]=count_press_ZF[id_key].b.HH4;
 chksum.bitmap+=Tx_Message[3];
 
 Tx_Message[4]=count_press_ZF[id_key].b.HL3;
 chksum.bitmap+=Tx_Message[4];

 Tx_Message[5]=count_press_ZF[id_key].b.LH2;
 chksum.bitmap+=Tx_Message[5];

 Tx_Message[6]=count_press_ZF[id_key].b.LL1;
 chksum.bitmap+=Tx_Message[6];   
 
 Tx_Message[7] = chksum.byte.HB;//
       
 Tx_Message[8] =chksum.byte.LB;//
 


 CDC_Transmit_FS(Tx_Message, 9);//putUSBUSART(Tx_Message,9);


      
}
 

 void Send_ACK_answer(uint8_t cmd){
  Tx_Message[0] =cmd;//
  Tx_Message[1]=ACK_ANSWER;//count_press[id].b.HH4;
  CDC_Transmit_FS(Tx_Message,2);//putUSBUSART(Tx_Message,9);

 }
/******************************************************************************
 * Function:       SetLeds()
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        
 *                  
 *
 * Note:            None
 *****************************************************************************/
void Tx_Keys(void)
{   
    uint8_t n;
    uint8_t key_code;
    uint16_t mask=0x0001;
    //if(num_keys_pressed>1){//probably MODIFIERS pressed
    //if(num_keys_pressed!=0){
    keys_tx_old[0].bitmap=keys_tx[0].bitmap;
    keys_tx[0].bitmap=0x0000;
    for(n=0;n<num_keys_pressed;n++){ 
       key_code= keys_multi[n];
       mask=0x0001;
       mask=mask<<n;
       switch(key_code){
                            case (KEY_F1):
                                                    keys_tx[0].b.b0=1;
                                                    break;
                            case (KEY_F2):
                                                    keys_tx[0].b.b1=1;
                                                    break;
                            case (KEY_F3):
                                                    keys_tx[0].b.b2=1;
                                                    break;
                            case (KEY_F4):
                                                    keys_tx[0].b.b3=1;
                                                    break;
                            case (KEY_F5):
                                                    keys_tx[0].b.b4=1;
                                                    break;
                            case (KEY_F6):
                                                    keys_tx[0].b.b5=1;
                                                    break;   
                            case (KEY_F7):
                                                    keys_tx[0].b.b6=1;
                                                    break;
                            case (KEY_F8):
                                                    keys_tx[0].b.b7=1;
                                                    break;
                            case (KEY_F9):
                                                    keys_tx[0].b.b8=1;
                                                    break;
                            case (KEY_F10):
                                                    keys_tx[0].b.b9=1;
                                                    break;
                            case (KEY_F11):
                                                    keys_tx[0].b.b10=1;
                                                    break;
                            case (KEY_F12):
                                                    keys_tx[0].b.b11=1;
                                                    break;      
                            case (KEY_F13):
                                                    keys_tx[0].b.b12=1;
                                                    break;  
                            case (KEY_F14):
                                                    keys_tx[0].b.b13=1;
                                                    break;
                            case (KEY_F15):
													keys_tx[0].b.b14=1;
													break;
                            case (KEY_F16):
												    keys_tx[0].b.b15=1;
												    break;
                           default:
                                                   //NUMLOCK_LED2_SetLow(); 
                                                  
                                                   break;
        }

       
    }
    if((keys_tx[0].bitmap!=0) && (keys_tx_old[0].bitmap!=keys_tx[0].bitmap)){
     //  Set_Buzzer(BEEP_SHORT_PERIOD,1); 
    } 
    Send_data_UART(); 
  //}
}


void Tx_Keys_ZF(void)
{
    uint8_t n;
    uint8_t key_code,pos;
    uint16_t mask=0x0001;
    //if(num_keys_pressed>1){//probably MODIFIERS pressed
    //if(num_keys_pressed!=0){

    for(n=0;n<3;n++){
     keys_tx_old[n].bitmap=keys_tx[n].bitmap;
     keys_tx[n].bitmap=0x0000;
    }

    for(n=0;n<num_keys_pressed;n++){
       key_code= keys_multi[n];

       if(key_code!= KEY_NONE){
    	    mask=0x0001;

    	    if(key_code<0x10){
    	    	 pos=(key_code-1);
    	    	 mask=mask<<pos;
    	    	 keys_tx[0].bitmap=(keys_tx[0].bitmap|mask);
    	      }else if(key_code<0x20){
    	    	      pos=(key_code-0x11);
    	    	 	  mask=mask<<pos;
    	    	      keys_tx[1].bitmap=(keys_tx[1].bitmap|mask);
    	            }else if(key_code<0x30){
    	            	    pos=(key_code-0x21);
    	            	    mask=mask<<pos;
    	            	    keys_tx[2].bitmap=(keys_tx[2].bitmap|mask);
    	                  }
       }

    }
   // if((keys_tx[0].bitmap!=0) && (keys_tx_old[0].bitmap!=keys_tx[0].bitmap)){
     //  Set_Buzzer(BEEP_SHORT_PERIOD,1);
   // }
    Send_data_UART();
  //}
}


void Tx_Counter_ZF(void)
{
	uint8_t i;
 keys_word_t chksum;
 uint8_t id_key,index;
 chksum.bitmap=0;
 index=0;
 Tx_Message[index] =START_BYTE_ALL_COUNTER_ZF;//
 chksum.bitmap+=Tx_Message[index++];
 for(i=0;i<NMAX_KEYPRESSED_TEST;i++){
	  if(config_id_key[i] < (NUM_MAX_KEY_ZF-1)){
         //Send_count_answer(config_id_key[i]);
          id_key=config_id_key[i];
		  Tx_Message[index]=id_key;//count_press[id].b.HH4;
		  chksum.bitmap+=Tx_Message[index++];

		  Tx_Message[index]=0;//count_press[id].b.HH4;
		  chksum.bitmap+=Tx_Message[index++];

		  Tx_Message[index]=count_press_ZF[id_key].b.HH4;
		  chksum.bitmap+=Tx_Message[index++];

		  Tx_Message[index]=count_press_ZF[id_key].b.HL3;
		  chksum.bitmap+=Tx_Message[index++];

		  Tx_Message[index]=count_press_ZF[id_key].b.LH2;
		  chksum.bitmap+=Tx_Message[index++];

		  Tx_Message[index]=count_press_ZF[id_key].b.LL1;
		  chksum.bitmap+=Tx_Message[index++];


	  }

  }

  //Send_count_answer((NUM_MAX_KEY_ZF-1));
  id_key=(NUM_MAX_KEY_ZF-1);
  Tx_Message[index]=id_key;//count_press[id].b.HH4;
  chksum.bitmap+=Tx_Message[index++];

  Tx_Message[index]=machine_status.bitmap;//count_press[id].b.HH4;
  chksum.bitmap+=Tx_Message[index++];

  Tx_Message[index]=count_press_ZF[id_key].b.HH4;
  chksum.bitmap+=Tx_Message[index++];

  Tx_Message[index]=count_press_ZF[id_key].b.HL3;
  chksum.bitmap+=Tx_Message[index++];

  Tx_Message[index]=count_press_ZF[id_key].b.LH2;
  chksum.bitmap+=Tx_Message[index++];

  Tx_Message[index]=count_press_ZF[id_key].b.LL1;
  chksum.bitmap+=Tx_Message[index++];

  Tx_Message[index++] = chksum.byte.HB;//
  Tx_Message[index++] =chksum.byte.LB;//
  CDC_Transmit_FS(Tx_Message, index);//

}

void Read_sensor(void){
//GPIO_PinState status;
uint8_t id;
for(id=0;id<3;id++){
	switch(id){
	  case(0)://SENSOR_HALL2 DOWN
			           //status=HAL_GPIO_ReadPin(HALL_SENSOR2_GPIO_Port, HALL_SENSOR2_Pin);
		               sensor.b.DOWN_OLD=sensor.b.HALL_DOWN;
		               if(HAL_GPIO_ReadPin(HALL_SENSOR2_GPIO_Port, HALL_SENSOR2_Pin)==SENSOR_HALL_CLOSE){
		            	   sensor.b.HALL_DOWN=1;
		               }else{
		            	   sensor.b.HALL_DOWN=0;
		               }
			           break;
	  case(1)://SENSOR_HALL1 UP
		               //status=HAL_GPIO_ReadPin(HALL_SENSOR1_GPIO_Port, HALL_SENSOR1_Pin);
		               sensor.b.UP_OLD=sensor.b.HALL_UP;
					   if(HAL_GPIO_ReadPin(HALL_SENSOR1_GPIO_Port, HALL_SENSOR1_Pin)==SENSOR_HALL_CLOSE){
						   sensor.b.HALL_UP=1;
					   }else{
						   sensor.b.HALL_UP=0;
					   }
	  			       break;
	  case(2)://BTN1
		               //status=HAL_GPIO_ReadPin(BTN1_GPIO_Port, BTN1_Pin);
					   if(HAL_GPIO_ReadPin(BTN1_GPIO_Port, BTN1_Pin)==SENSOR_HALL_OPEN){
						   sensor.b.BTN1=1;
					   }else{
						   sensor.b.BTN1=0;
					   }
	                   if(HAL_GPIO_ReadPin(LEVA_JOYSTICK_SEL_GPIO_Port, LEVA_JOYSTICK_SEL_Pin)==SENSOR_HALL_OPEN){
	 						   sensor.b.JOY_LEVA=1;
	 						   test_type=ZF_LEVA_TEST;//
	 				   }else{
	 						   sensor.b.JOY_LEVA=0;
	 						   test_type=ZF_JOYSTICK_TEST;//
	 				        }

	  			       break;

	  default:
		      break;
	}
}
}
/******************************************************************************
 * Function:       KeyboardScanRead(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        
 *                  
 *
 * Note:            None
 *****************************************************************************/
void KeyboardScanRead(void)
{
  uint8_t nrow,ncol;
  uint16_t pos_key=0;
 
  num_keys_pressed=0;  
  SetColsInput(0x00);
  for(nrow=0;nrow< NUM_ROW_ZF_MAX;nrow++){
      SetRowsOutput(nrow);
      //scan_row_delay=2;
      //while(scan_row_delay>0){};
      for(ncol=0;ncol < NUM_COL_ZF_MAX;ncol++){
           //key_val
           pos_key =(nrow*NUM_COL_ZF_MAX)+ncol;
           kb_status[nrow][ncol].old=kb_status[nrow][ncol].val;        
           kb_status[nrow][ncol].val=ReadColStatus(ncol);
           if (kb_status[nrow][ncol].old!=kb_status[nrow][ncol].val){
        	   kb_status[nrow][ncol].deb_ms=0;
           }else{
        	      kb_status[nrow][ncol].deb_ms++;
        	      if (kb_status[nrow][ncol].deb_ms == KEY_PRESSED_DEBOUNCE_MS){
        	    	  if (kb_status[nrow][ncol].val==KEY_PRESSED_VALUE){
        	    					   if(test_type==ZF_LEVA_TEST){//LEVA
        	    						   Check_Btn_Pressed_ZF(KEYBOARD_ZF_LEVA_SCANCODE[pos_key]);
        	    					   }else{
        	    						   Check_Btn_Pressed_ZF(KEYBOARD_ZF_JOY_SCANCODE[pos_key]);
        	    					   }
        	    	  }else{
        	    			 kb_status[nrow][ncol].deb_ms=0;
        	    		   }

				  }else if (kb_status[nrow][ncol].deb_ms > KEY_PRESSED_DEBOUNCE_MS){
					  kb_status[nrow][ncol].deb_ms = KEY_PRESSED_DEBOUNCE_MS;
				  }

                }

//           if (kb_status[nrow][ncol].val==KEY_PRESSED_VALUE){
//               if(num_keys_pressed<MAX_TASTI_CONTEMPORANEI){
//            	   if(test_type==ZF_LEVA_TEST){//LEVA
//                       keys_multi[num_keys_pressed]=KEYBOARD_ZF_LEVA_SCANCODE[pos_key];//KEYBOARD1_SCANCODE[pos_key];//
//            	   }else{
//            		   keys_multi[num_keys_pressed]=KEYBOARD_ZF_JOY_SCANCODE[pos_key];//KEYBOARD1_SCANCODE[pos_key];//
//            	   }
//                  num_keys_pressed++;
//               }
//           }
         
      } 
    // SetColsInput(0xFF);
    // scan_row_delay=2;
    // while(scan_row_delay>0){};
  }  
  SetRowsOutput(NUM_ROW_MAX+1);
    
}



/*******************************************************************************
 End of File
*/
